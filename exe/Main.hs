module Main where

------------------------------------------------------------------------------
import           Control.Monad
import           Control.Monad.IO.Class
import           Options.Applicative
import           Text.PrettyPrint.ANSI.Leijen (string)
------------------------------------------------------------------------------
import           NixDevelop.Commands.Build
import           NixDevelop.Commands.Docs
import           NixDevelop.Commands.Init
import qualified NixDevelop.Commands.OldBuild as OB
import           NixDevelop.Commands.Shell
import           NixDevelop.NdApp
import           NixDevelop.NixShell
import           NixDevelop.ShellEnv
import           NixDevelop.Types.NdCommand
import           NixDevelop.Types.GlobalOptions
------------------------------------------------------------------------------

data AllOpts = AllOpts
    { aoGlobals :: GlobalOptions
    , aoCmd :: NdCommand
    } deriving (Eq,Ord,Show,Read)

optParser :: Parser AllOpts
optParser = AllOpts <$> globalOpts <*> commandOpt

commandOpt :: Parser NdCommand
commandOpt = subparser $
    buildMod <>
    docsMod <>
    initMod <>
    OB.oldBuildMod <>
    shellMod <>
    command "refresh" refreshInfo <>
    command "repl" replInfo <>
    command "test" testInfo
  where
    refreshInfo = info (pure Refresh)
      (progDesc "Refresh the nix environment cache, running nix-shell if necessary")
    replInfo = info (pure Repl) (progDesc "Load project into a repl")
    testInfo = info (pure Test) (progDesc "Run project test suite")

main :: IO ()
main = do
    AllOpts g cmd <- customExecParser p opts
    runNdApp (_globalOpts_logLevel g) $ do
      logStr Debug $ "Running " ++ show cmd
      case cmd of
        Build os -> buildCommand os
        Docs os -> docsCommand os
        Init -> liftIO $ initCommand
        OldBuild os -> liftIO $ OB.oldBuildCommand os
        Refresh -> void reloadNixShellEnv
        Repl -> logStr Fatal "Not implemented yet"
        Shell os -> shellCommand os
        Test -> void $ nixShell_ ["--run", "echo hello"]
  where
    opts = info (optParser <**> helper) mods
    mods = progDesc "Nix Development Tool"
        <> footerDoc (Just theFooter)
    theFooter = string $ unlines
      [ "Run the following command to enable tab completion:"
      , ""
      , "source <(nd --bash-completion-script `which nd`)"
      ]
    p = prefs showHelpOnEmpty
