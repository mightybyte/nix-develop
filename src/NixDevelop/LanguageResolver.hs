module NixDevelop.LanguageResolver where

------------------------------------------------------------------------------
import           Control.Monad
import qualified Data.Set as S
import           System.Directory
import           System.FilePath
------------------------------------------------------------------------------
import           NixDevelop.Types.Language
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- | Figures out what language is being used in a given project directory.
-- This function does not recurse up.
resolveLanguage :: FilePath -> IO (Maybe Language)
resolveLanguage directory = do
  files <- listDirectory directory
  let extensions = S.fromList $ map takeExtension files
  if (S.member ".cabal" extensions)
    then return (Just Haskell)
    else return Nothing


------------------------------------------------------------------------------
-- | Runs a function at the specified directory.  If it does not return a
-- result, then it recurses up to the parent directory and continues the
-- process.  Returns Nothing if it reaches the root without success.
recurseUpDirectories :: FilePath -> (FilePath -> IO (Maybe a)) -> IO (Maybe a)
recurseUpDirectories directory f = do
    isDir <- doesDirectoryExist directory
    when (not isDir) $ error "Error: called recurseUpDirectories with a file!"
    go (splitDirectories directory)
  where
    go [] = return Nothing
    go ds = do
      res <- f (joinPath $ reverse ds)
      case res of
        Nothing -> go (tail ds)
        Just a  -> return (Just a)
