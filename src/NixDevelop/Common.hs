module NixDevelop.Common where

------------------------------------------------------------------------------
import System.Directory
import System.FilePath
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- | Recurses up from the current directory until the specified file is found.
recurseUpTo :: FilePath -> IO (Maybe FilePath)
recurseUpTo stopFile = do
    curdir <- getCurrentDirectory
    go curdir
  where
    go dir = do
      files <- listDirectory dir
      if stopFile `elem` files
        then return $ Just dir
        else if dir == "/" then return Nothing else go (takeDirectory dir)
