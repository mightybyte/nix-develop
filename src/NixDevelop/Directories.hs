{-|

There are two levels of filesystem storage in nix-develop: project and user.
In each of these storage levels there are two main types of data being
stored: cache and config.

The project directory is called @.nd@ and lives in the project root (usually
where .git is). The project cache directory @.nd/cache@ is for data that you
don't want to be checked in to version control. The project config directory
@.nd/config@ is for project-specific nix-develop config that you do want to
check in to version control.  (Currently nothing is being stored there.)

The user storage has both a config and cache location, but instead of putting
them in a single nd parent directory, we follow the [XDG Base Directory
Specifcation](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html).
The user config is usually @$SHELL/.config/nd@. The user cache is currently
unused. See above XDG link for more detail.

-}

module NixDevelop.Directories
  ( getUserConfigDir
  , getProjCacheDir
  , getProjDir
  , getNdDir
  ) where

------------------------------------------------------------------------------
import           Control.Monad.Trans
import           Data.Maybe
import           System.Directory
import           System.Environment
import           System.FilePath
------------------------------------------------------------------------------
import           NixDevelop.Globals
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- | Gets the full path to the nd user config directory.
getUserConfigDir :: IO FilePath
getUserConfigDir = do
    mcfg <- liftIO $ lookupEnv "XDG_CONFIG_HOME"
    cfg <- case mcfg of
      Nothing -> (</> ".config") <$> liftIO getHomeDirectory
      Just cfg -> return cfg
    let d = cfg </> "nd"
    createDirectoryIfMissing True d
    return d

------------------------------------------------------------------------------
-- | Gets the full path to the nd project cache directory, creating it if
-- necessary.
getProjCacheDir :: IO FilePath
getProjCacheDir = do
    ndDir <- getNdDir
    return (ndDir </> "cache")

------------------------------------------------------------------------------
-- | Gets the full path to the nd project directory, creating it if necessary.
getNdDir :: IO FilePath
getNdDir = do
    pd <- getProjDir
    let d = pd </> ".nd"
    createDirectoryIfMissing True d
    return d

------------------------------------------------------------------------------
-- | Gets the full path to the nd project directory, creating it if necessary.
getProjDir :: IO FilePath
getProjDir = do
    curDir <- getCurrentDirectory
    fromMaybe curDir <$> findProjectDirectory curDir

findProjectDirectory :: FilePath -> IO (Maybe FilePath)
findProjectDirectory "/" = return Nothing
findProjectDirectory dir = do
    configExists <- doesFileExist (dir </> configFilename)
    if configExists
      then return $ Just dir
      else do
        dirExists <- doesDirectoryExist (dir </> ".nd")
        if dirExists
          then return $ Just dir
          else do
            gitExists <- doesDirectoryExist (dir </> ".git")
            if gitExists
              then return (Just dir)
              else findProjectDirectory (takeDirectory dir)
