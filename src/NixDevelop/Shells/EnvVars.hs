{-# LANGUAGE OverloadedStrings #-}

module NixDevelop.Shells.EnvVars
  ( askEnvCacheFile
  , getEnvSaveCmd
  , envScript
  ) where

------------------------------------------------------------------------------
import           Control.Monad.Reader
import           Data.List
import           Data.Text (Text)
import qualified Data.Text as T
import           System.Directory
import           System.FilePath
import           Text.Printf
------------------------------------------------------------------------------
import           NixDevelop.Directories
import           NixDevelop.Types.NdApp
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- | Gets the full path to the nix-shell environment cache file
askEnvCacheFile :: NdApp FilePath
askEnvCacheFile = do
    d <- liftIO getProjCacheDir
    liftIO $ createDirectoryIfMissing True d
    return (d </> envCacheFilename)
  where
    envCacheFilename = "nix-shell-env"

getEnvSaveCmd :: NdApp String
getEnvSaveCmd = do
    cf <- askEnvCacheFile
    return $ "export -p > " <> cf

-- NOTE We don't have a corresponding function to save the env vars because that
-- has to be done inside a real pure non-cached nix-shell (i.e. bash) and to
-- minimize the nix-shell performance penalty we save both the aliases and the
-- environment variables at the same time in `ShellEnv.reloadNixShellEnv`.

envScript :: Bool -> NdApp Text
envScript isPure = do
    cf <- askEnvCacheFile
    let restoreCmd = "source " <> cf
    return $ T.pack $ if isPure
      then restoreCmd
      else intercalate "; " [saveOrig, restoreCmd, addOrig, clearOrig]
  where
    origPath = "ND_SAVED_ORIG_PATH"
    saveOrig = printf "export %s=$PATH" origPath
    addOrig = "export PATH=$PATH:$" <> origPath
    clearOrig = "unset " <> origPath
