{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module NixDevelop.Shells.Zsh where

------------------------------------------------------------------------------
import           Control.Monad
import           Control.Monad.Reader
import qualified Data.Map as M
import           Data.Text (Text)
import qualified Data.Text.IO as T
import           NeatInterpolation
import           System.Directory
import           System.FilePath
import           System.Process
------------------------------------------------------------------------------
import           NixDevelop.NdApp
import           NixDevelop.Shells.Common
import           NixDevelop.Shells.ZshPrimitives
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.OsShellOptions
------------------------------------------------------------------------------

getShellCP :: OsShellOptions -> NdApp CreateProcess
getShellCP oso = do
    cmd <- getShellCmd
    rcDir <- takeDirectory <$> getShellRcFile
    logStr Debug $ "Running zsh with ZDOTDIR=" <> rcDir

    startupCmd <- getStartupCmds oso

    let envMap0 = M.insert "ZDOTDIR" rcDir mempty
    let envMap1 = M.insert "ND_CMD_TO_RUN" startupCmd envMap0
    logText Detail "Running zsh will the following options:"
    logStr Detail cmd
    logStr Detail (show envMap1)
    return $ (shell cmd) { env = Just (M.toList envMap1) }

getShellRcFile :: NdApp FilePath
getShellRcFile = do
    rcfile <- getZshRcFile
    exists <- liftIO $ doesFileExist rcfile
    when (not exists) $ writeRcArtifacts rcfile
    return rcfile

getZshRcFile :: NdApp FilePath
getZshRcFile = do
    pdir <- asks _ndConfig_projDir
    let d = pdir </> "config" </> "zsh"
    let rcfile = d </> ".zshrc"
    exists <- liftIO $ doesFileExist rcfile
    if exists
      then return rcfile
      else do
        ucdir <- asks _ndConfig_userConfigDir
        return $ ucdir </> "zsh" </> ".zshrc"

writeRcArtifacts :: FilePath -> NdApp ()
writeRcArtifacts fp = liftIO $ do
    createDirectoryIfMissing True (takeDirectory fp)
    T.writeFile fp zshRcTemplate

-- | Beginnings of zsh support.
--
-- TODO This doesn't work great yet, but is better than nothing. It at least is
-- able to run shellHook.
zshRcTemplate :: Text
zshRcTemplate = [text|
source ~/.zshrc
set +e;
eval "$$shellHook"

# IMPORTANT: Do not modify this line! This is the mechanism that nix-develop
# uses to set the environment for `nd shell`.
eval "$$ND_CMD_TO_RUN"
|]
