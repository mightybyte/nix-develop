{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module NixDevelop.Shells.Common where

------------------------------------------------------------------------------
import           Data.List
import           Data.Text (Text)
import qualified Data.Text as T
import           NeatInterpolation
------------------------------------------------------------------------------
import           NixDevelop.Shells.EnvVars
import           NixDevelop.Shells.Aliases
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.OsShellOptions
------------------------------------------------------------------------------

-- | Currently this function works with both zsh and bash.  The environment
-- and aliases saved by reloadNixShellEnv can be loaded as-is by both shells.
-- We may eventually need the loading of this data to be customized to support
-- other shells.  That can be done on an as-needed basis.
getStartupCmds :: OsShellOptions -> NdAppT IO String
getStartupCmds oso = do
    let run = maybe [] (\c -> [c, "exit"]) $ osoRun oso
    ascript <- aliasScript (osoPure oso)
    escript <- envScript (osoPure oso)
    let envCmds = [T.unpack ascript, T.unpack escript]
    return $ intercalate "; " $ envCmds ++ run

------------------------------------------------------------------------------
-- | Note, instead of running `source $stdenv/setup` we're cherry-picking
-- things from that script because it really slows things down.
--
-- TODO We might need an out-of-the-box option to use the full setup script.
commonRcTemplate :: Text
commonRcTemplate = [text|
_eval() {
    if [ "$$(type -t "$$1")" = function ]; then
        set +u
        "$$@" # including args
    else
        set +u
        eval "$$1"
    fi
    # `run*Hook` reenables `set -u`
}
_callImplicitHook() {
    set -u
    local def="$$1"
    local hookName="$$2"
    case "$$(type -t "$$hookName")" in
        (function|alias|builtin)
            set +u
            "$$hookName";;
        (file)
            set +u
            source "$$hookName";;
        (keyword) :;;
        (*) if [ -z "$${!hookName:-}" ]; then
                return "$$def";
            else
                set +u
                eval "$${!hookName}"
            fi;;
    esac
    # `_eval` expects hook to need nounset disable and leave it
    # disabled anyways, so Ok to to delegate. The alternative of a
    # return trap is no good because it would affect nested returns.
}
runHook() { # This function taken from nix's $$stdenv/setup
    local oldOpts="$$(shopt -po nounset)"
    set -u # May be called from elsewhere, so do `set -u`.

    local hookName="$$1"
    shift
    local hooksSlice="$${hookName%Hook}Hooks[@]"

    local hook
    # Hack around old bash being bad and thinking empty arrays are
    # undefined.
    for hook in "_callImplicitHook 0 $$hookName" $${!hooksSlice+"$${!hooksSlice}"}; do
        _eval "$$hook" "$$@"
        set -u # To balance `_eval`
    done

    eval "$${oldOpts}"
    return 0
}


set +e;
[ -n "$$PS1" ] && PS1='\n\[\033[1;32m\][nix-shell:\w]\$\[\033[0m\] ';
if [ "$(type -t runHook)" = function ]; then runHook shellHook; fi;

# IMPORTANT: Do not modify this line! This is the mechanism that nix-develop
# uses to set the environment for `nd shell`.
eval "$$ND_CMD_TO_RUN"
|]
