{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module NixDevelop.Shells.ZshPrimitives where

------------------------------------------------------------------------------
import           Data.Text (Text)
import           Text.Printf
------------------------------------------------------------------------------
import           NixDevelop.Types.NdApp
------------------------------------------------------------------------------

getShellCmd :: NdApp String
getShellCmd = do
    return "/bin/zsh"

clearAliases :: Text
clearAliases = "unalias -m '*'"

saveAliases :: Text
saveAliases = "alias -L"

setVar :: String -> String -> String
setVar varname value =
    printf "export %s=%s; fi" varname value

setVarIfUnset :: String -> String -> String
setVarIfUnset varname value =
    printf "if [ -z ${%s+x} ]; then export %s=%s; fi" varname varname value

prependVar :: String -> String -> String
prependVar varname value =
    printf "export %s=%s:%s" varname value varname
