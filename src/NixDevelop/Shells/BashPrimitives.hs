{-# LANGUAGE OverloadedStrings #-}

module NixDevelop.Shells.BashPrimitives where

------------------------------------------------------------------------------
import Data.Text (Text)
------------------------------------------------------------------------------

clearAliases :: Text
clearAliases = "unalias -a"

saveAliases :: Text
saveAliases = "alias -p"
