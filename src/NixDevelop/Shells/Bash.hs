{-# LANGUAGE OverloadedStrings #-}

module NixDevelop.Shells.Bash where

------------------------------------------------------------------------------
import           Control.Monad
import           Control.Monad.Reader
import qualified Data.Text as T
import qualified Data.Text.IO as T
import           System.Directory
import           System.FilePath
import           System.Process
------------------------------------------------------------------------------
import           NixDevelop.Shells.Common
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.OsShellOptions
------------------------------------------------------------------------------

getShellCmd :: NdApp String
getShellCmd = do
    bash <- liftIO getNixBash
    rcfile <- getShellRcFile
    return $ unwords [bash, "--rcfile", rcfile]

getShellCP :: OsShellOptions -> NdApp CreateProcess
getShellCP oso = do
    cmd <- getShellCmd

    startupCmd <- getStartupCmds oso

    return $ (shell cmd) { env = Just $ [("ND_CMD_TO_RUN", startupCmd)] }

getShellRcFile :: NdApp FilePath
getShellRcFile = do
    rcfile <- getBashRcFile
    exists <- liftIO $ doesFileExist rcfile
    when (not exists) $ writeRcArtifacts rcfile
    return rcfile

getBashRcFile :: NdApp FilePath
getBashRcFile = do
    pdir <- asks _ndConfig_projDir
    let d = pdir </> "config" </> "bash"
    let rcfile = d </> "bashrc"
    exists <- liftIO $ doesFileExist rcfile
    if exists
      then return rcfile
      else do
        ucdir <- asks _ndConfig_userConfigDir
        return $ ucdir </> "bash" </> "bashrc"

writeRcArtifacts :: FilePath -> NdApp ()
writeRcArtifacts fp = liftIO $ do
    createDirectoryIfMissing True (takeDirectory fp)
    T.writeFile fp commonRcTemplate

------------------------------------------------------------------------------
getNixBash :: IO FilePath
getNixBash = do
    -- TODO Cache the result of this in $HOME/.cache/nd
    (_,o,_,_) <- runInteractiveCommand
      "nix-build -E '(import <nixpkgs> {}).bashInteractive'"
    dir <- T.unpack . T.strip <$> T.hGetContents o
    return $ dir </> "bin" </> "bash"
