{-# LANGUAGE OverloadedStrings #-}

module NixDevelop.Shells.Aliases
  ( askAliasCacheFile
  , getAliasSaveCmd
  , aliasScript
  ) where

------------------------------------------------------------------------------
import           Control.Monad.Reader
import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text as T
import           System.Directory
import           System.FilePath
import           Text.Printf
------------------------------------------------------------------------------
import           NixDevelop.Directories
import           NixDevelop.Shells.Primitives
import           NixDevelop.Types.NdApp
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- | Gets the full path to the nix-shell environment cache file
askAliasCacheFile :: NdApp FilePath
askAliasCacheFile = do
    d <- liftIO getProjCacheDir
    liftIO $ createDirectoryIfMissing True d
    return (d </> envCacheFilename)
  where
    envCacheFilename = "nix-shell-aliases"

getAliasSaveCmd :: NdApp String
getAliasSaveCmd = do
    cf <- askAliasCacheFile
    return $ printf "alias -p > %s" cf

-- NOTE We don't have a corresponding function to save the aliases because that
-- has to be done inside a real pure non-cached nix-shell (i.e. bash) and to
-- minimize the nix-shell performance penalty we save both the aliases and the
-- environment variables at the same time in `ShellEnv.reloadNixShellEnv`.

aliasScript :: Bool -> NdApp Text
aliasScript isPure = do
    clear <- clearAliasesCmd
    fp <- askAliasCacheFile
    let ccmd = if isPure then Just clear else Nothing
    return $ T.intercalate "; " $ catMaybes [ccmd, Just ("source " <> T.pack fp)]
