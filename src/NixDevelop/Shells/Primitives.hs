{-# LANGUAGE OverloadedStrings   #-}

{-|

Top-level module unifying shell operations in a way that is shell-agnostic.

-}

module NixDevelop.Shells.Primitives where

------------------------------------------------------------------------------
import           Control.Applicative
import           Control.Monad.Reader
import           Data.Maybe
import           Data.Text (Text)
------------------------------------------------------------------------------
import qualified NixDevelop.Shells.BashPrimitives as Bash
import qualified NixDevelop.Shells.ZshPrimitives as Zsh
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.ProjConfig
import           NixDevelop.Types.UserConfig
import           NixDevelop.Types.Shell
------------------------------------------------------------------------------

getShell :: NdApp Shell
getShell = do
    pshell <- asks (_ucShell . _pcUserConfig . _ndConfig_projCfg)
    ushell <- asks (_ucShell . _ndConfig_userCfg)
    return $ fromMaybe Bash (pshell <|> ushell)

-- | Outputs a command that will output the shell aliases in a form suitable
-- for running later.
clearAliasesCmd :: NdApp Text
clearAliasesCmd = do
    sh <- getShell
    case sh of
      Bash -> return Bash.clearAliases
      Zsh -> return Zsh.clearAliases

-- | Outputs a command that will output the shell aliases in a form suitable
-- for running later.
saveAliasesCmd :: NdApp Text
saveAliasesCmd = do
    sh <- getShell
    case sh of
      Bash -> return Bash.saveAliases
      Zsh -> return Zsh.saveAliases
