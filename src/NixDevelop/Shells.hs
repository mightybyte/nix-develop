{-# LANGUAGE OverloadedStrings   #-}

{-|

Top-level module unifying shell operations in a way that is shell-agnostic.

-}

module NixDevelop.Shells where

------------------------------------------------------------------------------
import           System.Process
------------------------------------------------------------------------------
import qualified NixDevelop.Shells.Bash as Bash
import qualified NixDevelop.Shells.Zsh as Zsh
import qualified NixDevelop.Shells.ZshPrimitives as Zsh
import           NixDevelop.Shells.Primitives
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.OsShellOptions
import           NixDevelop.Types.Shell
------------------------------------------------------------------------------

getShellCP :: OsShellOptions -> NdApp CreateProcess
getShellCP oso = do
    sh <- getShell
    case sh of
      Bash -> Bash.getShellCP oso
      Zsh -> Zsh.getShellCP oso

getShellCmd :: NdApp String
getShellCmd = do
    sh <- getShell
    case sh of
      Bash -> Bash.getShellCmd
      Zsh -> Zsh.getShellCmd
