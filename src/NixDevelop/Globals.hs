module NixDevelop.Globals where

------------------------------------------------------------------------------
-- | The main nd config file.  All project configuration should be kept here.
configFilename :: FilePath
configFilename = "nd.nix"
