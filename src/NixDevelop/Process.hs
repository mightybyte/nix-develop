{-# LANGUAGE TemplateHaskell #-}

module NixDevelop.Process where

------------------------------------------------------------------------------
import           Control.Lens
import           System.IO
import           System.Process
------------------------------------------------------------------------------

-- | Simplifies working with processes
data Process = Process
  { _procStdin :: Maybe Handle
  , _procStdout :: Maybe Handle
  , _procStderr :: Maybe Handle
  , _procHandle :: ProcessHandle
  }

makeLenses ''Process

startProcess :: CreateProcess -> IO Process
startProcess cp = do
  (i,o,e,h) <- createProcess cp
  return $ Process i o e h
