module NixDevelop.Types.ShellOptions where

------------------------------------------------------------------------------
import           Data.Default
------------------------------------------------------------------------------

data ShellOptions = ShellOptions
    { soRun :: Maybe String
    , soPure :: Bool
    , soReload :: Bool
    } deriving (Eq,Ord,Read,Show)

instance Default ShellOptions where
    def = ShellOptions def False False
