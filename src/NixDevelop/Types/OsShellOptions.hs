module NixDevelop.Types.OsShellOptions where

data OsShellOptions = OsShellOptions
    { osoRun :: Maybe String
    , osoPure :: Bool
    } deriving (Eq,Ord,Show)
