module NixDevelop.Types.GlobalOptions where

------------------------------------------------------------------------------
import           Data.Default
import           Data.Maybe
import           Options.Applicative
------------------------------------------------------------------------------
import           NixDevelop.Types.LogLevel
------------------------------------------------------------------------------

newtype GlobalOptions = GlobalOptions
    { _globalOpts_logLevel :: LogLevel
    } deriving (Eq,Ord,Read,Show)

instance Default GlobalOptions where
    def = GlobalOptions def

globalOpts :: Parser GlobalOptions
globalOpts = GlobalOptions <$> (fromMaybe Info <$> logLevelOpt)
