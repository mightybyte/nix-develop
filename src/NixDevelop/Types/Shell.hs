{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE UndecidableInstances  #-}

module NixDevelop.Types.Shell where

------------------------------------------------------------------------------
import           Control.Monad.Free
import           Data.Text (Text)
import           Nix
import           Nix.Convert
import           Nix.Effects
------------------------------------------------------------------------------

data Shell = Bash | Zsh
  deriving (Eq,Ord,Show,Read,Enum,Bounded)

parseShell :: Text -> Maybe Shell
parseShell "bash" = Just Bash
parseShell "zsh" = Just Zsh
parseShell _ = Nothing

instance (Convertible e m, MonadEffects m) => FromValue Shell m (NValueNF m) where
    fromValueMay = \case
        Free (NVStrF ns) -> do
          pure $ parseShell $ principledStringIgnoreContext ns
        _ -> pure Nothing
    fromValue v = fromValueMay v >>= \case
        Just b -> pure b
        _ -> throwError $ ExpectationNF (TString NoContext) v
