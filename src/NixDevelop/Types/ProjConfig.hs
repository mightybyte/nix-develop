{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE UndecidableInstances  #-}
module NixDevelop.Types.ProjConfig
  ( ProjConfig(..)
  ) where

------------------------------------------------------------------------------
import           Control.Monad.Free
import           Data.Text (Text)
import           Data.Vector (Vector)
import qualified Data.Vector as V
import           Nix
import           Nix.Convert
import           Nix.Effects
------------------------------------------------------------------------------
import           Data.Default
import           NixDevelop.Types.UserConfig
import           NixDevelop.Utils
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- | This is the data type representing the parsed contents of nd.nix.
data ProjConfig = ProjConfig
    { _pcBuildLevels :: Vector Text
    , _pcUserConfig :: UserConfig
    } deriving (Eq,Ord,Show,Read)

instance Default ProjConfig where
    def = ProjConfig mempty def

instance (Convertible e m, MonadEffects m) => FromValue ProjConfig m (NValueNF m) where
    fromValueMay = \case
        v@(Free (NVSetF s _)) -> do
          ls <- optionalSetLookup mempty "buildLevels" s
          uc <- fromValue v
          pure $ Just $ ProjConfig
            (V.fromList $ map principledStringIgnoreContext ls)
            uc
        _ -> pure Nothing
    fromValue v = fromValueMay v >>= \case
        Just b -> pure b
        _ -> throwError $ ExpectationNF TSet v
