module NixDevelop.Types.DocsOptions where

import Data.Default

data DocsOptions = DocsOptions
    { docsArgs         :: String
    } deriving (Eq,Ord,Read,Show)

instance Default DocsOptions where
    def = DocsOptions mempty

--data DocServeOptions = DocServeOptions
--    { dsoPort            :: Int
--    , dsoOpenBrowser     :: Bool
--    , dsoCompilerPackage :: String
--    , dsoPackages        :: [String]
--    } deriving (Eq,Ord,Read,Show)
--
--instance Default DocServeOptions where
--    def = DocServeOptions 8080 False "haskellPackages" mempty
