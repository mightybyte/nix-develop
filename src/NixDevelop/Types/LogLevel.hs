{-# LANGUAGE LambdaCase #-}

module NixDevelop.Types.LogLevel where

------------------------------------------------------------------------------
import           Data.Default
import           Options.Applicative
------------------------------------------------------------------------------

------------------------------------------------------------------------------
data LogLevel
    = Debug
    -- ^ Output meant to help debug problems in nix-develop
    | Detail
    -- ^ Messages that normal users might want to see, but have enough volume
    -- that it might be unwanted clutter
    | Info
    -- ^ Normal information that users should see
    | Warn
    -- ^ Something might be wrong but it probably won't influnce outcome
    | Error
    -- ^ Computation can continue but might not give you the results you want
    | Fatal
    -- ^ Fatal errors that cannot be recovered from
  deriving (Eq, Ord, Show, Read, Enum, Bounded)

instance Default LogLevel where
    def = Info

showLogLevel :: LogLevel -> String
showLogLevel Debug = "debug"
showLogLevel Detail = "detail"
showLogLevel Info = "info"
showLogLevel Warn = "warn"
showLogLevel Error = "error"
showLogLevel Fatal = "fatal"

parseLogLevel :: String -> Maybe LogLevel
parseLogLevel = \case
  "debug" -> Just Debug
  "detail" -> Just Detail
  "info" -> Just Info
  "warn" -> Just Warn
  "error" -> Just Error
  "fatal" -> Just Fatal
  _ -> Nothing

logLevelOpt :: Parser (Maybe LogLevel)
logLevelOpt = optional $ option (maybeReader parseLogLevel) $ mconcat
  [ long "log-level"
  , short 'l'
  , completeWith (map showLogLevel [minBound .. maxBound])
  , help "Log level (options: debug, detail, info, warn, error, fatal)"
  ]
