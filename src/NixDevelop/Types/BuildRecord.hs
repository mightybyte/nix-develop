{-# LANGUAGE OverloadedStrings   #-}
module NixDevelop.Types.BuildRecord where

------------------------------------------------------------------------------
import           Control.Monad
import           Control.Monad.Trans
import           Data.Aeson
import           Data.Aeson.Encoding
import           Data.Aeson.Types
import qualified Data.ByteString as B
import           Data.Map (Map)
import           Data.String.Conv
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Time
import           System.Directory
import           System.Exit
import           System.FilePath
import           Text.Read (readMaybe)
------------------------------------------------------------------------------
import           NixDevelop.Directories
import           NixDevelop.NdApp
import qualified NixDevelop.Types.LogLevel as L
import           NixDevelop.Types.NdApp
------------------------------------------------------------------------------

data BuildOrigin = UserBuild | AutomaticBuild
  deriving (Eq,Ord,Show,Read,Enum,Bounded)

boToText :: BuildOrigin -> Text
boToText UserBuild = "user"
boToText AutomaticBuild = "automatic"

textToBo :: Text -> Parser BuildOrigin
textToBo "user" = return UserBuild
textToBo "automatic" = return AutomaticBuild
textToBo s = fail $ T.unpack s ++ " is not a valid BuildOrigin"

instance ToJSON BuildOrigin where
    toJSON = String . boToText

instance FromJSON BuildOrigin where
    parseJSON (String t) = textToBo t
    parseJSON invalid = typeMismatch "Coord" invalid

instance ToJSONKey BuildOrigin where
    toJSONKey = ToJSONKeyText boToText (text . boToText)

instance FromJSONKey BuildOrigin where
    fromJSONKey = FromJSONKeyTextParser textToBo

data BuildRecord = BuildRecord
    { _buildRecordTimestamp :: UTCTime
    , _buildRecordOrigin    :: BuildOrigin
    , _buildRecordLevel     :: Int
    , _buildRecordExitCode  :: Maybe ExitCode
    } deriving (Eq,Ord,Show,Read)

instance ToJSON BuildRecord where
    toJSON br = case _buildRecordExitCode br of
                  Nothing -> object base
                  Just ec -> object $
                    ("exitcode"  .= show ec) : base
      where
        base =
          [ "timestamp" .= _buildRecordTimestamp br
          , "origin"    .= _buildRecordOrigin br
          , "level"     .= _buildRecordLevel br
          ]

instance FromJSON BuildRecord where
    parseJSON (Object o) = BuildRecord
      <$> o .: "timestamp"
      <*> o .: "origin"
      <*> o .: "level"
      <*> (join . fmap readMaybe <$> o .:? "exitcode")
    parseJSON invalid = typeMismatch "BuildRecord" invalid

buildRecordFile :: FilePath
buildRecordFile = "last-build"

loadBuildRecords :: NdApp (Map BuildOrigin BuildRecord)
loadBuildRecords = do
    ebrs <- loadBuildRecordsEither
    case ebrs of
      Left e -> do
        logStr L.Error $ "Could not parse build records! " ++ show e
        return mempty
      Right bs -> return bs

loadBuildRecordsEither :: NdApp (Either String (Map BuildOrigin BuildRecord))
loadBuildRecordsEither = do
    d <- liftIO getProjCacheDir
    let f = d </> buildRecordFile
    exists <- liftIO $ doesFileExist f
    if exists
      then do
        bs <- liftIO $ B.readFile f
        return $ eitherDecodeStrict bs
      else return (Right mempty)

saveBuildRecords :: Map BuildOrigin BuildRecord -> NdApp ()
saveBuildRecords brs = do
    d <- liftIO getProjCacheDir
    liftIO $ do
      createDirectoryIfMissing True d
      B.writeFile (d </> buildRecordFile) (toS $ encode brs)
