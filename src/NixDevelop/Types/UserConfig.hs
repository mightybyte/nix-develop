{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE UndecidableInstances  #-}
module NixDevelop.Types.UserConfig
  ( UserConfig(..)
  ) where

------------------------------------------------------------------------------
import           Control.Monad.Free
import           Data.Default
import qualified Data.HashMap.Lazy as M
import           Data.Vector (Vector)
import qualified Data.Vector as V
import           Nix
import           Nix.Convert
import           Nix.Effects
------------------------------------------------------------------------------
import           NixDevelop.Utils
import           NixDevelop.Types.NixCache
import           NixDevelop.Types.Shell
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- | This is the data type representing the parsed contents of config.nix.
data UserConfig = UserConfig
    { _ucCaches :: Vector NixCache
    , _ucShell :: Maybe Shell
    } deriving (Eq,Ord,Show,Read)

instance Default UserConfig where
    def = UserConfig mempty def

instance (Convertible e m, MonadEffects m) => FromValue UserConfig m (NValueNF m) where
    fromValueMay = \case
        Free (NVSetF s _) -> do
          sh <- maybe (return Nothing) fromValueMay (M.lookup "shell" s)
          cs <- optionalSetLookup mempty "caches" s
          pure $ Just $ UserConfig
            (V.fromList cs)
            sh
        _ -> pure Nothing
    fromValue v = fromValueMay v >>= \case
        Just b -> pure b
        _ -> throwError $ ExpectationNF TSet v
