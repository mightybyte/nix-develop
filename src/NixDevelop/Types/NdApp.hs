{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module NixDevelop.Types.NdApp where

------------------------------------------------------------------------------
import           Control.Monad.Reader
import           Data.Aeson (Value)
------------------------------------------------------------------------------
import           NixDevelop.Types.ProjConfig
import           NixDevelop.Types.LogLevel
import           NixDevelop.Types.UserConfig
------------------------------------------------------------------------------

data NdConfig = NdConfig
    { _ndConfig_logLevel :: LogLevel
    , _ndConfig_curDir :: FilePath
    , _ndConfig_userConfigDir :: FilePath
    , _ndConfig_userCfg :: UserConfig
    , _ndConfig_projDir :: FilePath
    , _ndConfig_projCfg :: ProjConfig
    , _ndConfig_nixCfg :: Maybe Value
--    , _ndConfig_bashPath :: FilePath
    } deriving (Eq,Show)

type NdApp a = NdAppT IO a

newtype NdAppT m a = NdAppT { _unNdAppT :: ReaderT NdConfig m a }
  deriving (Functor, Applicative, Monad, MonadTrans, MonadIO,
            MonadReader NdConfig)
