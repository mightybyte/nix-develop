module NixDevelop.Types.NdCommand where

------------------------------------------------------------------------------
import qualified NixDevelop.Types.BuildOptions as Build
import           NixDevelop.Types.DocsOptions
import qualified NixDevelop.Types.OldBuildOptions as OldBuild
import qualified NixDevelop.Types.ShellOptions as Shell
------------------------------------------------------------------------------

data NdCommand
  = Build Build.BuildOptions
  | Docs DocsOptions
  | Init
  | OldBuild OldBuild.BuildOptions
  | Refresh
  | Repl
  | Shell Shell.ShellOptions
  | Test
  deriving (Eq,Ord,Read,Show)

{-

Potential command names:

----- Coding / Development Phase -----
tc          Fastest build that doesn't generate code
build       Doesn't genrate code but doesn't have -Wall and other checks that are required by CI
repl
-----
prep
push
deploy
test
----- Other Tasks -----
docs


-}
