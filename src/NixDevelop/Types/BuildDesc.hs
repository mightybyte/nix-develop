{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}

module NixDevelop.Types.BuildDesc where

------------------------------------------------------------------------------
import           Data.Text (Text)
import           Data.Typeable
import           Nix.Convert
import           Nix.Effects
import           Nix.Frames
import           Nix.String
import           Nix.Value
------------------------------------------------------------------------------

mkStrValue :: Text -> NixString
mkStrValue = principledMakeNixStringWithoutContext

------------------------------------------------------------------------------
-- | Describes the state of an option that can either be disabled, enabled, or
-- enabled and run.  Applies to things like tests and benchmarks.
data TernaryBuildFlag = TDisable | TEnable | TRun
  deriving (Eq,Ord,Show,Read,Enum,Bounded)

parseTernaryBuildFlag :: Text -> Maybe TernaryBuildFlag
parseTernaryBuildFlag "tdisable" = Just TDisable
parseTernaryBuildFlag "tbuild" = Just TEnable
parseTernaryBuildFlag "trun" = Just TRun
parseTernaryBuildFlag _ = Nothing

instance Applicative m => ToValue TernaryBuildFlag m (NValue m) where
    toValue TDisable = toValue $ mkStrValue "tdisable"
    toValue TEnable = toValue $ mkStrValue "tbuild"
    toValue TRun = toValue $ mkStrValue "trun"

instance Applicative m => ToValue TernaryBuildFlag m (NValueNF m) where
    toValue TDisable = toValue $ mkStrValue "tdisable"
    toValue TEnable = toValue $ mkStrValue "tbuild"
    toValue TRun = toValue $ mkStrValue "trun"

instance (Convertible e m, MonadEffects m) => FromValue TernaryBuildFlag m (NValueNF m) where
    fromValueMay v = do
      (\mv -> parseTernaryBuildFlag . principledStringIgnoreContext =<< mv) <$> fromValueMay v
    fromValue v = fromValueMay v >>= \case
        Just b -> pure b
        _ -> throwError $ ExpectationNF (TString NoContext) v

------------------------------------------------------------------------------
-- | Optimization levels O0 through O3.  These seem pretty standard across
-- most compilers.
data Optimization = O0 | O1 | O2 | O3
  deriving (Eq,Ord,Show,Read,Enum,Bounded)

instance Applicative m => ToValue Optimization m (NValue m) where
    toValue O0 = toValue $ mkStrValue "O0"
    toValue O1 = toValue $ mkStrValue "O1"
    toValue O2 = toValue $ mkStrValue "O2"
    toValue O3 = toValue $ mkStrValue "O3"

instance Applicative m => ToValue Optimization m (NValueNF m) where
    toValue O0 = toValue $ mkStrValue "O0"
    toValue O1 = toValue $ mkStrValue "O1"
    toValue O2 = toValue $ mkStrValue "O2"
    toValue O3 = toValue $ mkStrValue "O3"

instance (Convertible e m, MonadEffects m) => FromValue Optimization m (NValueNF m) where
    fromValueMay v =
      return . parseOptimization . principledStringIgnoreContext =<< fromValue v
    fromValue v = fromValueMay v >>= \case
        Just b -> pure b
        _ -> throwError $ ExpectationNF (TString NoContext) v

parseOptimization :: Text -> Maybe Optimization
parseOptimization "O0" = Just O0
parseOptimization "O1" = Just O1
parseOptimization "O2" = Just O2
parseOptimization "O3" = Just O3
parseOptimization _ = Nothing

------------------------------------------------------------------------------
-- | Describes all the information needed to do a build.
data BuildDesc = BuildDesc
    { bdCompiler          :: Text
    , bdTypeCheck         :: Bool
    , bdOptimization      :: Maybe Optimization
    , bdAllWarnings       :: Bool
    , bdWarningsAreErrors :: Bool
    , bdProfiling         :: Bool
    , bdTests             :: TernaryBuildFlag
    , bdBenchmarks        :: TernaryBuildFlag
    , bdLint              :: Bool
    , bdDocs              :: Bool
    , bdCoverage          :: Bool
    --, bdRunPerfRegress :: Bool
    } deriving (Eq,Ord,Show,Read,Typeable)

-- instance Monad m => ToValue BuildDesc m (NValueNF m) where
--     toValue BuildDesc{..} = do
--       a <- toValue bdCompiler
--       b <- toValue bdTypeCheck
--       c <- maybe (toValue O1) toValue bdOptimization
--       d <- toValue bdAllWarnings
--       e <- toValue bdWarningsAreErrors
--       f <- toValue bdProfiling
--       g <- toValue bdTests
--       h <- toValue bdBenchmarks
--       i <- toValue bdLint
--       j <- toValue bdDocs
--       k <- toValue bdCoverage
--       let s = M.fromList
--             [ ("compiler", a)
--             , ("typecheck", b)
--             , ("optimization", c)
--             , ("allWarnings", d)
--             , ("warningsAreErrors", e)
--             , ("profiling", f)
--             , ("tests", g)
--             , ("benchmarks", h)
--             , ("lint", i)
--             , ("docs", j)
--             , ("coverage", k)
--             ]
--       pure $ Fix $ NVSetF s M.empty
--
-- yn y n = undefined
