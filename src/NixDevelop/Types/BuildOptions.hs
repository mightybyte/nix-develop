module NixDevelop.Types.BuildOptions where

------------------------------------------------------------------------------
import Data.Default
import Numeric.Natural
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- | Strongly typed representation of the build command's first command-line
-- argument.
data BuildTarget = BuildLevel Natural | BuildComponent String
  deriving (Eq,Ord,Show,Read)

data BuildOptions = BuildOptions
    { buildOptionsTarget :: Maybe BuildTarget
    } deriving (Eq,Ord,Show,Read)

instance Default BuildOptions where
    def = BuildOptions Nothing
