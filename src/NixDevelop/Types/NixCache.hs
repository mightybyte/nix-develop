{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE UndecidableInstances  #-}
module NixDevelop.Types.NixCache where

------------------------------------------------------------------------------
import           Control.Error
import           Control.Lens
import           Control.Monad.Free
import           Data.Bifunctor
import qualified Data.HashMap.Lazy as M
import           Data.String.Conv
import           Data.Text (Text)
import           Nix
import           Nix.Convert
import           Nix.Effects
import           URI.ByteString
------------------------------------------------------------------------------
import           NixDevelop.Utils
------------------------------------------------------------------------------

------------------------------------------------------------------------------
data CacheProtocol = Ssh | Http | Https
  deriving (Eq,Ord,Show,Read,Enum)

cacheProtocolText :: CacheProtocol -> Text
cacheProtocolText Ssh = "ssh"
cacheProtocolText Http = "http"
cacheProtocolText Https = "https"

parseCacheProtocol :: String -> Either String CacheProtocol
parseCacheProtocol = \case
  "ssh" -> Right Ssh
  "http" -> Right Http
  "https" -> Right Https
  s -> Left $ "Could not parse protocol: " ++ s

instance (Convertible e m, MonadEffects m) => FromValue CacheProtocol m (NValueNF m) where
    fromValueMay = \case
        Free (NVStrF ns) -> do
          pure $ case principledStringIgnoreContext ns of
            "ssh" -> Just Ssh
            "http" -> Just Http
            "https" -> Just Https
            _ -> Nothing
        _ -> pure Nothing
    fromValue v = fromValueMay v >>= \case
        Just b -> pure b
        _ -> throwError $ ExpectationNF TSet v

protocolPort :: CacheProtocol -> Int
protocolPort Ssh = 22
protocolPort Http = 80
protocolPort Https = 443

data HostPort = HostPort
    { hpHost :: String
    , hpPort :: Int
    } deriving (Eq,Ord,Show,Read)

urlToHostPort :: Text -> Either (String, Text) HostPort
urlToHostPort url = first (,url) $ do
    uriRef <- first show $ parseURI strictURIParserOptions $ toS url
    scheme <- parseCacheProtocol $ toS $ view (uriSchemeL . schemeBSL) uriRef
    authority <- note "URL is missing authority section" $ view authorityL uriRef
    let host = toS $ view (authorityHostL . hostBSL) authority
    let port = maybe (protocolPort scheme) portNumber $ authorityPort authority
    return $ HostPort host port

data NixCache = NixCache
    { nixCacheUrl :: Text
    , nixCacheKey :: Maybe Text
    , nixCacheHostPort :: HostPort
    -- ^ This is here to force fail-fast parse failures on construction and
    -- avoid the need to handle Maybes later.  The original url is also kept
    -- so we can do the right thing with respect to Nix's slash handling.
    } deriving (Eq,Ord,Show,Read)

urlToNixCache :: Text -> Either (String, Text) NixCache
urlToNixCache url = do
    hp <- urlToHostPort url
    return $ NixCache url Nothing hp

unsafeMkNixCache :: Text -> Text -> NixCache
unsafeMkNixCache url k = NixCache url (Just k) hp
  where
    Right hp = urlToHostPort url

unsafeUrlToNixCache :: Text -> NixCache
unsafeUrlToNixCache url = c
  where
    Right c = urlToNixCache url

------------------------------------------------------------------------------
instance (Convertible e m, MonadEffects m) => FromValue NixCache m (NValueNF m) where
    fromValueMay = \case
        Free (NVSetF s _) -> do
          k <- maybe (return Nothing) (fmap Just . fromValue) $ M.lookup "key" s
          runMaybeT $ do
            url <- principledStringIgnoreContext <$> setLookup "url" s
            hp <- hoistMaybe $ hush $ urlToHostPort url
            return $ NixCache
              url
              (principledStringIgnoreContext <$> k)
              hp
        _ -> pure Nothing
    fromValue v = fromValueMay v >>= \case
        Just b -> pure b
        _ -> throwError $ ExpectationNF TSet v

defaultNixCache :: NixCache
defaultNixCache = NixCache
  "https://cache.nixos.org/"
  (Just "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=")
  (HostPort "cache.nixos.org" 443)
