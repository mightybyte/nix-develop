module NixDevelop.Types.OldBuildOptions where

import Data.Default

data BuildOptions = BuildOptions
    { buildOptionsDebug  :: Bool
    , buildOptionsArgs   :: [String]
    } deriving (Eq,Ord,Read,Show)

instance Default BuildOptions where
    def = BuildOptions False mempty
