module NixDevelop.Types.Language where

------------------------------------------------------------------------------
import Data.Default
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- | Might make this completely dynamic in the future so new languages can be
-- defined without needing to recompile.  But this approach is fine for now.
data Language = Haskell
  deriving (Eq,Ord,Show,Read,Enum,Bounded)

------------------------------------------------------------------------------
data LanguageConfig = LanguageConfig
    { _lcCallPackage :: String
    } deriving (Eq,Ord,Show,Read)

instance Default LanguageConfig where
  def = LanguageConfig "callPackage"

------------------------------------------------------------------------------
-- | A mapping holding all the config options for each language.  Eventually
-- this should probably be moved to a ile read at runtime, but this is quick
-- and easy for now and gives us totality checking and avoids needing to deal
-- with Maybes.
globalLanguageConfig :: Language -> LanguageConfig
globalLanguageConfig Haskell = LanguageConfig "haskellPackages.callPackage"
