module NixDevelop.Types.LogMessage where

------------------------------------------------------------------------------
import           Data.Text (Text)
------------------------------------------------------------------------------
import           NixDevelop.Types.LogLevel
------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- | Text annotated with the LogLevel for appropriate error handling later.
data LogMessage = LogMessage
  { logMessageLevel :: LogLevel
  , logMessageText  :: Text
  } deriving (Eq, Ord, Show, Read)

msg :: LogLevel -> Text -> LogMessage
msg = LogMessage
