{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module NixDevelop.ShellEnv where

------------------------------------------------------------------------------
import           Control.Lens
import           Control.Monad.Reader
import           Data.Default
import           System.Directory
import           System.Environment
import           System.Exit
import           System.Process
------------------------------------------------------------------------------
import           NixDevelop.NixShell
import           NixDevelop.Process
import           NixDevelop.Shells.EnvVars
import           NixDevelop.Types.NdApp
import           NixDevelop.Shells.Aliases
------------------------------------------------------------------------------

-- Old code for decoding direnv dumps
-- ------------------------------------------------------------------------------
-- -- | Convert the text of a direnv dump to an alist suitable to pass to env
-- -- field of 'CreateProcess'.
-- decodeDump :: Text -> Either String (HashMap Text Text)
-- decodeDump t = do
--     zipped <- B64.decode $ encodeUtf8 $ T.strip t
--     let jsonBS = decompress $ LBS.fromStrict zipped
--     --case jsonBS ^? _Object of
--     case A.decode jsonBS of
--       Just (A.Object _) -> Right $ HM.fromList (jsonBS ^@.. members . _String)
--       _ -> Left "Dump must be a valid JSON object"
--
-- ------------------------------------------------------------------------------
-- -- | Convert an environment variable alist of the sort returned by
-- -- 'getEnvironment' to the direnv environment dump format.
-- encodeDump :: HashMap Text Text -> Text
-- encodeDump m = decodeUtf8 b64
--   where
--     o = map (\(k,v) -> (k, A.String v)) $ HM.toList m
--     json = A.encode $ A.object o
--     zipped = compress json
--     b64 = B64.encode $ LBS.toStrict zipped

-- Old code for parsing bash double quoted strings
-- -- | Returns a tuple of the quoted string (including the quotes) and the rest of
-- -- the string. If the first character is not a double quote, then the first item
-- -- in the tuple will be the empty string. Returns Nothing if the string ends
-- -- without a closing double quote.
-- --
-- -- NOTE: This function does not attempt to do any kind of un-quoting of the
-- -- quoted string. It only identifies the extent of the quoted string.
-- splitQuoted :: Text -> Maybe (Text,Text)
-- splitQuoted str =
--     case T.uncons str of
--       Just ('"', t) -> go 1 t
--       _ -> Just ("", str)
--   where
--     go i t =
--       case T.uncons t of
--         Nothing -> Nothing
--         Just ('"', _) -> Just $ T.splitAt (i+1) str
--         Just ('\\', t2) ->
--           case T.uncons t2 of
--             Just ('"', t3) -> go (i+2) t3
--             _ -> go (i+1) t2
--         Just (_, t2) -> go (i+1) t2

-- | Should not be called from a nix-shell.
reloadNixShellEnv :: NdApp (Either String ())
reloadNixShellEnv = do
    -- This doesn't need to be in Shells.hs with shell-specific code because
    -- nix-shell --pure always uses bash. We have to do it this way because
    -- another call to nd will start a new shell, losing the defined aliases.
    ecmd <- getEnvSaveCmd
    acmd <- getAliasSaveCmd
    let opts = def & nsoPure .~ True
                   & nsoRun .~ Just (ecmd <> "; " <> acmd)
    p <- nixShell opts
    code <- liftIO $ waitForProcess (_procHandle p)
    case code of
      ExitSuccess -> return $ Right ()
      ExitFailure _ ->
        return $ Left $ "Failure getting env inside nix-shell"


------------------------------------------------------------------------------
-- | When not running in a nix-shell, this function dumps the nix-shell env to
-- files so the shell can be entered quickly. When in nix shell, an error is
-- returned.
reloadShellEnvIfNecessary :: NdApp (Either String ())
reloadShellEnvIfNecessary = do
    e <- liftIO getEnvironment
    case lookup "IN_NIX_SHELL" e of
      Nothing -> do
        envCacheExists <- liftIO . doesFileExist =<< askEnvCacheFile
        aliasCacheExists <- liftIO . doesFileExist =<< askAliasCacheFile
        if not $ envCacheExists && aliasCacheExists
          then reloadNixShellEnv
          else return $ Right ()

      -- Error here because if we are already in a nix-shell, we cannot get
      -- the correct list of aliases because nd will have already been
      -- executed in a new process and the list of aliases will have been
      -- reset.
      Just _ -> return $ Left "Cannot load shell environment from inside a nix shell"
