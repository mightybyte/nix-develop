{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

{-|

Low-level nix-shell wrappers. These probably won't be used in very many places
because the `nd shell` functionalty is so much faster in the typical case.

These wrappers handle things like:

- Binary cache management
- Anything that directly involves args passed to nix-shell

-}

module NixDevelop.NixShell
  ( nixShell_
  , NixShellOpts
  , nsoPure
  , nsoRun

  -- Exporting lenses and def instead of accessors and constructors to make
  -- the API more stable.

  , nixShell

  , nixWrapper
  , NixCmd(..)

  -- Only exported for the test suite
  , partitionTrusted
  ) where

------------------------------------------------------------------------------
import           Control.Concurrent
import           Control.Error
import           Control.Exception
import           Control.Lens
import           Control.Monad.Reader
import           Data.Aeson
import           Data.Aeson.Lens
import           Data.Default
import           Data.IORef
import           Data.List
import           Data.String.Conv
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Vector as V
import           Network.Socket hiding (Debug)
import           System.Environment
import           System.Exit
import           System.Posix.User
import           System.Process
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Text.Printf
------------------------------------------------------------------------------
import           NixDevelop.NdApp
import           NixDevelop.Process
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.NixCache
import           NixDevelop.Types.ProjConfig
import           NixDevelop.Types.UserConfig
------------------------------------------------------------------------------

data NixShellOpts = NixShellOpts
  { _nsoPure :: Bool
  , _nsoRun :: Maybe String
  } deriving (Eq,Ord,Show,Read)

makeLenses ''NixShellOpts

-- | The 'Default' instance represents the options you get when you invoke
-- nix-shell with no arguments.
instance Default NixShellOpts where
  def = NixShellOpts False Nothing

nixShell :: NixShellOpts -> NdApp Process
nixShell nso = do
  let args = concat
        [ if _nsoPure nso then ["--pure"] else []
        , maybe [] (\c -> ["--run", c]) (_nsoRun nso)
        ]
  nixWrapper NixShell args

nixShell_ :: [String] -> NdApp ExitCode
nixShell_ args = do
  p <- nixWrapper NixShell args
  liftIO $ waitForProcess $ _procHandle p

data NixCmd = NixShell | NixBuild
  deriving (Eq,Ord,Show,Read,Enum,Bounded)

nixCmdString :: NixCmd -> String
nixCmdString NixShell = "nix-shell"
nixCmdString NixBuild = "nix-build"

nixWrapper
  :: NixCmd
  -> [String]
  -> NdApp Process
nixWrapper nixCmd userArgs = do
  logDocExact Debug $
    PP.string (printf "nixWrapper %s %s" (show nixCmd) (show userArgs)) <>
    PP.line
  mNixCfg <- asks _ndConfig_nixCfg
  inNixShell <- liftIO $ lookupEnv "IN_NIX_SHELL"
  case (mNixCfg, inNixShell) of
    -- These undefineds are fine because `logStr Fatal` will already exit
    (Nothing, Just _) -> do
      logStr Fatal $ printf "Can't call %s inside a nix shell" (nixCmdString nixCmd)
      return undefined
    (Nothing, Nothing) -> do
      logStr Fatal "Can't load the nix config.  Do you have the nix binary in your path?"
      return undefined
    (Just nixcfg, _) -> nixWrapper' nixcfg nixCmd userArgs

-- TODO Come up with better name before exporting this
nixWrapper'
  :: Value
  -> NixCmd
  -> [String]
  -> NdApp Process
nixWrapper' nixcfg nixCmd userArgs = do
  ndCfg <- ask
  let pcfg = _ndConfig_projCfg ndCfg
  trustedUser <- liftIO $ isTrustedUser nixcfg
  let allProjCaches = V.toList $ _ucCaches $ _pcUserConfig pcfg

  projCaches <-
    if trustedUser
      -- Trusted users can use any caches. Untrusted users can only use trusted
      -- caches.
      then return allProjCaches
      else do
        let (tpc, upc) = partitionTrusted nixcfg allProjCaches
        when (not $ null upc) $ do
          logText Warn $ "Not using the following untrusted caches: " <>
                         T.intercalate ", " (map nixCacheUrl upc)
          nixConfDir <- fromMaybe "/etc/nix" <$> liftIO (lookupEnv "NIX_CONF_DIR")
          logDocExact Warn $ PP.string $ unlines
            [ printf "To fix this, edit %s/nix.conf and do one of two things:" nixConfDir
            , "  1. Add the public keys for these caches to 'trusted-public-keys'"
            , "  2. Add your user to 'trusted-users'"
            , ""
            ]
          logDocExact Warn $ PP.fillSep $ map PP.text $ words $ unwords
            [ "NOTE: #1 will fix this problem for only these caches. "
            , "#2 will fix this problem for all future caches but is"
            , "dangerous and essentially equivalent to giving your user"
            , "root access to the whole system.  See 'man nix.conf' for"
            , "more information."
            ]
          logDocExact Warn PP.line
          logDocExact Warn PP.line
        return tpc

  let (badSys, sysCaches) = partitionEithers $ systemCaches nixcfg

  when (not $ null badSys) $ logDocExact Fatal $ PP.string $ unlines $
    "The following caches defined in nix.conf could not be parsed:" :
    map (\(e,u) -> printf "%s (%s)" (T.unpack u) e) badSys

  let caches = projCaches ++ sysCaches

  good <- availableCaches caches
  let bad = filter (\c -> not (c `elem` good)) caches

  when (not $ null bad) $ do
    logDoc Warn $ PP.hang 0 $ PP.fillSep $
      "No response from the following caches:" :
      PP.punctuate "," (map (PP.text . hpHost . nixCacheHostPort) bad)
    logDocExact Warn PP.line
    logStrExact Warn "      Continuing without them..."
    logDocExact Warn PP.line
  when (not $ null good) $
    logStr Detail $ "These caches will be used: " ++
                    intercalate ", " (map (hpHost . nixCacheHostPort) good)

  let cacheArgs = case good of
        [] -> ["--no-substitute"]
        cs -> [ "--option"
              , "substituters"
              , T.unpack $ T.unwords $ map nixCacheUrl cs
              ]
      pureArg = if nixCmd == NixShell then ["--pure"] else []
      args = pureArg ++ cacheArgs ++ userArgs

  logCmd Debug "nixWrapper running: " (nixCmdString nixCmd) args

  liftIO $ startProcess $ CreateProcess
      { cmdspec = RawCommand (nixCmdString nixCmd) args
      , cwd = Nothing
      , env = Nothing
      , std_in = Inherit
      , std_out = Inherit
      , std_err = Inherit
      , close_fds = False
      , create_group = False
      , delegate_ctlc = False
      , detach_console = False
      , create_new_console = False
      , new_session = False
      , child_group = Nothing
      , child_user = Nothing
      , use_process_jobs = False
      }

--partitionM :: Monad m => (a -> m Bool) -> [a] -> m ([a],[a])
--partitionM mp xs = foldM select ([],[]) xs
--  where
--    select ~(ts,fs) x = do
--      p <- mp x
--      return $ if p then (x:ts, fs) else (ts, x:fs)

------------------------------------------------------------------------------
systemCaches :: Value -> [Either (String, Text) NixCache]
systemCaches v = map (urlToNixCache . toS) $ parseConfigList "substituters" v

parseConfigList :: Text -> Value -> [Text]
parseConfigList k v = v ^.. _Value . key k . key "value" . values . _String


availableCaches :: [NixCache] -> NdApp [NixCache]
availableCaches cs = do
    goodCaches <- liftIO $ newIORef []
    let forkAvail c = liftIO $ forkIO $ isCacheAvailable goodCaches c
    tids <- mapM forkAvail cs
    liftIO $ do
      threadDelay 800000
      mapM_ killThread tids
      readIORef goodCaches

isCacheAvailable :: IORef [NixCache] -> NixCache -> IO ()
isCacheAvailable goodList nc = withSocketsDo $ do
    _ :: Either SomeException () <- try $ do
      let HostPort host port = nixCacheHostPort nc
      addr <- resolve host (show port)
      sock <- open addr
      atomicModifyIORef' goodList (\a -> (nc : a,()))
      close sock
    return ()
  where
    resolve h p = do
        let hints = defaultHints { addrSocketType = Stream }
        addr:_ <- getAddrInfo (Just hints) (Just h) (Just p)
        return addr
    open addr = do
        sock <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
        connect sock $ addrAddress addr
        return sock

isTrustedUser :: Value -> IO Bool
isTrustedUser v = do
    let tus = trustedUsers v
    user <- getLoginName
    return $ T.pack user `elem` tus

trustedUsers :: Value -> [Text]
trustedUsers = parseConfigList "trusted-users"

trustedKeys :: Value -> [Text]
trustedKeys = parseConfigList "trusted-public-keys"

------------------------------------------------------------------------------
-- | When Nix check caches trailing slashes are significant and will make
-- otherwise correct caches not work.  This function partitions the caches but
-- instead of returning the input list, it returns the correctly slashed form.
partitionTrusted :: Value -> [NixCache] -> ([NixCache], [NixCache])
partitionTrusted v cs =
    foldl' single ([],[]) cs
  where
    ss = V.fromList $ parseConfigList "substituters" v
    tss = V.fromList $ parseConfigList "trusted-substituters" v
    allSs = ss <> tss
    tks = V.fromList $ trustedKeys v
    single (good,bad) c =
      case V.findIndex (cacheEq c) allSs of
        Nothing -> (good, c : bad)
        Just i ->
          case nixCacheKey c of
            Nothing -> (unsafeUrlToNixCache (allSs V.! i) : good, bad)
            Just k -> if k `V.elem` tks
                        then (unsafeMkNixCache (allSs V.! i) k : good, bad)
                        else (good, unsafeMkNixCache (allSs V.! i) k : bad)

    cacheEq c h = dropSlash (nixCacheUrl c) == dropSlash h
    dropSlash = T.dropWhileEnd (=='/')


--systemSubstituters :: Value -> Text -> [Text]
--systemSubstituters v k = k `elem` s
--  where
--    s = parseConfigList "substituters" v ++
--        parseConfigList "trusted-substituters" v

--parseNixInstantiate :: IO [Text]
--parseNixInstantiate = do
--    (_,_,e,_) <- runInteractiveCommand "nix-instantiate -vv"
--    out <- T.hGetContents e
--    return $ foldl' getFile [] $ T.lines out
--  where
--    getFile fs a
--      | T.isPrefixOf "evaluating file" a =
--        let f = T.takeWhile (/= '\'') $ T.drop 17 a
--         in f : fs
--      | T.isPrefixOf "copied source" a =
--        -- Has an ->
--        let (f,rest) = T.span (/= '\'') $ T.drop 15 a
--            g = T.takeWhile (/= '\'') $ T.drop 6 rest
--         in f : g : fs
--      | T.isPrefixOf "instantiated" a =
--        let f = T.takeWhile (/= '\'') $ T.drop 5 $ snd $ T.breakOn " -> '" a
--         in f : fs
--      | otherwise = fs
