module NixDevelop.Utils where

------------------------------------------------------------------------------
import           Control.Error
import           Data.Hashable
import qualified Data.HashMap.Lazy as M
import           Nix.Convert
------------------------------------------------------------------------------

setLookup
  :: (Monad m, FromValue b m v, Eq k, Hashable k)
  => k
  -> M.HashMap k v
  -> MaybeT m b
setLookup k s =
  MaybeT . fromValueMay =<< hoistMaybe (M.lookup k s)

optionalSetLookup
  :: (Monad m, FromValue a m v, Eq k, Hashable k)
  => a
  -> k
  -> M.HashMap k v
  -> m a
optionalSetLookup d k s =
  case M.lookup k s of
    Nothing -> return d
    Just v -> fromValue v
