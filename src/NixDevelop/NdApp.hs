{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module NixDevelop.NdApp
  ( runNdApp
  , getNdBinaryPath
  , logDocExact
  , logStrExact
  , logTextExact
  , logDoc
  , logDocs
  , logCmd
  , logStr
  , logStrWrap
  , logText
  , logMsg
  , module NixDevelop.Types.LogLevel
  ) where

------------------------------------------------------------------------------
import           Control.Error
import           Control.Monad.Catch
import           Control.Monad.Reader
import           Data.Aeson (Value(..), eitherDecodeStrict)
import qualified Data.ByteString as B
import           Data.Char
import           Data.Default
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Time
import           Nix hiding (NixLevel(..))
import           Nix.Convert
import           System.Console.ANSI.Types
import           System.Directory
import           System.Environment
import           System.Exit
import           System.FilePath
import           System.IO
import           System.Process
import           Text.PrettyPrint.ANSI.Leijen (Doc)
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import qualified Text.PrettyPrint.ANSI.Leijen.Internal as PP
import           Text.Printf
------------------------------------------------------------------------------
import           NixDevelop.Directories
import           NixDevelop.Globals
import           NixDevelop.Types.LogLevel
import           NixDevelop.Types.LogMessage
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.ProjConfig
import           NixDevelop.Types.UserConfig
------------------------------------------------------------------------------

------------------------------------------------------------------------------
runNdApp :: LogLevel -> NdApp a -> IO a
runNdApp logLevel app = do
    res <- runExceptT $ do
      enixCfg <- lift getNixConfig
      (curDir, projDir, projCfg) <- lift loadProject
      (ucfgDir, userCfg) <- lift loadUserConfig
      let cfg = NdConfig logLevel curDir ucfgDir userCfg projDir projCfg
                         (hush enixCfg)
      let wrapper = do
            logStr Detail $ printf "Project directory: %s" projDir
            logStr Detail $ printf "User config directory: %s" ucfgDir
            logStr Debug $ printf "userCfg: %s" (show userCfg)
            app
      lift $ runReaderT (_unNdAppT wrapper) cfg
    case res of
      Left e -> do
        PP.putDoc $ prependLevel Fatal $ PP.text e
        exitWith (ExitFailure 1)
      Right a -> return a

------------------------------------------------------------------------------
getNixConfig :: IO (Either String Value)
getNixConfig = do
  -- TODO Load this information directly so we don't have to rely on nix being
  -- in the path.
  (_,o,_,_) <- runInteractiveCommand "nix show-config --json"
  bs <- B.hGetContents o
  return $ eitherDecodeStrict bs

------------------------------------------------------------------------------
-- | Gets the full path to the nd executable
getNdBinaryPath :: IO FilePath
getNdBinaryPath = getExecutablePath >>= canonicalizePath

levelColor :: LogLevel -> Color
levelColor Debug = White
levelColor Detail = White
levelColor Info = White
levelColor Warn = Yellow
levelColor Error = Red
levelColor Fatal = Red

------------------------------------------------------------------------------
-- | Logs a plain Doc without adding a prefix indicating the log level.
logDocExact :: LogLevel -> Doc -> NdApp ()
logDocExact level doc = do
  threshold <- asks _ndConfig_logLevel
  when (level >= threshold) $ liftIO $ PP.displayIO stdout (PP.renderPretty 1 80 doc)
  when (level == Fatal) $ liftIO $ exitWith (ExitFailure 1)

------------------------------------------------------------------------------
-- | Logs a plain String without adding a prefix indicating the log level.
logStrExact :: LogLevel -> String -> NdApp ()
logStrExact level str = logDocExact level (PP.text str)

------------------------------------------------------------------------------
-- | Logs a plain Text without adding a prefix indicating the log level.
logTextExact :: LogLevel -> Text -> NdApp ()
logTextExact level str = logStrExact level (T.unpack str)

------------------------------------------------------------------------------
-- | Write a log message with a prefix indicating the log level.
logDoc :: LogLevel -> Doc -> NdApp ()
logDoc level doc = logDocExact level (prependLevel level doc)

------------------------------------------------------------------------------
-- | Write a log message with a prefix indicating the log level.
logDocs :: LogLevel -> [Doc] -> NdApp ()
logDocs level docs = logDocExact level (prependLevel level $ PP.fillSep docs)

------------------------------------------------------------------------------
-- | Log command executed in a shell with correct quoting to indicate args.
logCmd :: LogLevel -> String -> String -> [String] -> NdApp ()
logCmd level prefix cmd args =
    logDoc level $
      PP.text prefix <> PP.text (unwords $ map quote (cmd:args)) <> PP.line
  where
    quote s = if any isSpace s then "\"" <> s <> "\"" else s

------------------------------------------------------------------------------
prependLevel :: LogLevel -> Doc -> Doc
prependLevel level doc =
    PP.color (levelColor level) (PP.text $ show level) <>
    PP.text ": " <> doc <> PP.hardline

------------------------------------------------------------------------------
-- | Write a log message with a prefix indicating the log level.
logStr :: LogLevel -> String -> NdApp ()
logStr level str = logDoc level (PP.text str)

------------------------------------------------------------------------------
-- | Logs a plain String without adding a prefix indicating the log level.
logStrWrap :: LogLevel -> String -> NdApp ()
logStrWrap level str = logDoc level (PP.fillSep $ map PP.text $ words str)

------------------------------------------------------------------------------
-- | Write a log message with a prefix indicating the log level.
logText :: LogLevel -> Text -> NdApp ()
logText level str = logStr level (T.unpack str)

------------------------------------------------------------------------------
-- | Write a LogMessage
logMsg :: LogMessage -> NdApp ()
logMsg lm = logStr (logMessageLevel lm) (T.unpack $ logMessageText lm)

-- | Returns the current directory, the project directory and the project
-- config data structure.
loadProject :: IO (FilePath, FilePath, ProjConfig)
loadProject = do
    curDir <- getCurrentDirectory
    projDir <- getProjDir
    cfg <- do
      let fp = projDir </> configFilename
      exists <- doesFileExist fp
      if exists then loadNixFile fp else return def
    return $ (curDir, projDir, cfg)

-- | Returns the current directory, the project directory and the project
-- config data structure.
loadUserConfig :: IO (FilePath, UserConfig)
loadUserConfig = do
    ucfgDir <- getUserConfigDir
    cfg <- do
      let fp = ucfgDir </> configFilename
      exists <- doesFileExist fp
      if exists then loadNixFile fp else return def
    return $ (ucfgDir, cfg)

------------------------------------------------------------------------------
-- | Should only be used when you know the file exists.
loadNixFile
  :: FromValue a (Lazy IO) (NValueNF (Lazy IO))
  => FilePath
  -> IO a
loadNixFile fp = do
    t <- getCurrentTime
    let opts = defaultOptions t
    nvnf <- hnixEvalFile opts fp
    mres <- runLazyM opts $ fromValueMay nvnf
    case mres of
      Nothing -> do
        putStrLn $ "Error parsing " ++ fp
        runLazyM opts $ fromValue nvnf
      Just res -> return res

hnixEvalFile :: Options -> FilePath -> IO (NValueNF (Lazy IO))
hnixEvalFile opts file = do
    parseResult <- parseNixFileLoc file
    case parseResult of
      Failure e        ->
          error $ "Parsing failed for file `" ++ file ++ "`.\n" ++ show e
      Success expr -> do
          runLazyM opts $
              catch (evaluateExpression (Just file) nixEvalExprLoc
                                        normalForm expr) $ \case
                  NixException frames ->
                      errorWithoutStackTrace . show
                          =<< renderFrames @(NThunk (Lazy IO)) frames
