{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}

module NixDevelop.Commands.Docs where

------------------------------------------------------------------------------
import           Control.Monad.Trans
import           Data.List
import           Data.Semigroup ((<>))
import qualified Data.Text as T
import           Options.Applicative hiding (Success, Failure)
import qualified Turtle as Turtle
------------------------------------------------------------------------------
import           NixDevelop.NdShell
import           NixDevelop.Types.DocsOptions
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.NdCommand
------------------------------------------------------------------------------


------------------------------------------------------------------------------
docsMod :: Mod CommandFields NdCommand
docsMod = command "docs" $ info (Docs <$> parser <**> helper) i
  where
    parser = DocsOptions
      <$> (strArgument
             ( metavar "PACKAGE"
            <> help "Local package to include in the documentation" ))
    i = fullDesc
        <> header synopsis
        <> progDesc synopsis
    synopsis = "Look up documentation"


------------------------------------------------------------------------------
-- | Get documentation that is in scope.  If already in a nix-shell, we use
-- whatever is in that environment.  If not, we use the cached environment.
--
-- TODO Eventually this should detect the language(s) and dependent on that
docsCommand :: DocsOptions -> NdApp ()
docsCommand (DocsOptions package) = do
  mres <- ndShellOut $ "ghc-pkg describe " ++ package
  case mres of
    Left _ -> return ()
    Right (o,_) -> do
      let needle = "haddock-html:"
      case dropWhile (not . isPrefixOf needle) (lines o) of
        [] -> liftIO $ putStrLn $ "Unable to find documentation for " ++ package
        (p:ps) -> do
          case drop (length needle + 1) p of
            [] -> do
              case ps of
                [] -> liftIO $ putStrLn $ "Unable to find documentation for " ++ package
                (fp:_) -> handleFoundDocs fp
            fp -> handleFoundDocs fp
  return ()

handleFoundDocs :: Turtle.MonadIO m => FilePath -> m ()
handleFoundDocs fp = liftIO $ do
    putStrLn url
    openBrowser url
  where
    url = fp ++ "/index.html"

openBrowser :: Turtle.MonadIO m => String -> m ()
openBrowser url = do
  _ <- Turtle.shell ("open " <> (T.pack url)) (return mempty)
  return ()

-- ------------------------------------------------------------------------------
-- docServeMod :: Mod CommandFields NdCommand
-- docServeMod = command "docserve" $ info (DocServe <$> parser <**> helper) i
--   where
--     parser = DocServeOptions
--       <$> (fromMaybe 8080 <$> optional portOpt)
--       <*> (flag True False
--              ( short 'n'
--             <> long "no-browser"
--             <> help "Don't automatically open a browser window" ))
--       <*> (fmap (fromMaybe $ dsoCompilerPackage def) $ optional (strOption
--             ( short 'c'
--            <> long "compiler-package"
--            <> help "The compiler package root.  Examples: \"haskellPackages\", \"haskell.compiler.ghc861\", or \"ghc\" (with reflex-platform)")))
--       <*> (many $ strArgument
--              ( metavar "PACKAGES"
--             <> help "Local packages to include in the documentation" ))
--     i = fullDesc
--         <> header synopsis
--         <> progDesc synopsis
--     portOpt = option auto
--              ( short 'p'
--             <> long "port"
--             <> metavar "INT"
--             <> help "The port that the server will run on" )
--     synopsis = "Run a local documentation server"
--
--
-- ------------------------------------------------------------------------------
-- -- | Create a default.nix template in the current directory.
-- docServeCommand :: DocServeOptions -> IO ()
-- docServeCommand (DocServeOptions port browser cp packages) = do
--     hSetBuffering stdout NoBuffering
--     hasNix <- doesFileExist "default.nix"
--     let pstr = unwords $ map ("p."++) packages
--         nixExpr = if hasNix then "with import ./. {}; " else ("" :: String)
--         hoogleArgs = ["server", "-p", show port, "--local"]
--         args = [ "-p"
--                , printf "%s%s.ghcWithHoogle (p: [%s])" nixExpr cp pstr
--                , "--run"
--                , unwords ("hoogle" : hoogleArgs)
--                ]
--         url = printf "http://127.0.0.1:%d" port
--     printf "Serving documentation at: %s\n" url
--     inNixShellVar <- Turtle.need "IN_NIX_SHELL"
--     let inNixShell = isJust inNixShellVar
--     let runServer = void $
--           Turtle.procStrictWithErr "nix-shell" (map T.pack args) (return mempty)
--         openBrowser = do
--           Turtle.sleep 3
--           putStr "Opening browser in 3..."
--           Turtle.sleep 1
--           putStr "2..."
--           Turtle.sleep 1
--           putStr "1..."
--           Turtle.sleep 1
--           putStr "now\n"
--           _ <- Turtle.shell ("open " <> (T.pack url)) (return mempty)
--           return ()
--     if inNixShell
--       then putStrLn "Cannot run docserve inside a nix shell"
--       else do
--         putStrLn $ unwords ("nix-shell" : map surround args)
--         Turtle.sh $ Turtle.parallel
--           [runServer, if browser then openBrowser else return ()]
--     return ()
--   where
--     surround s = if null (filter isSpace s) then s else "\"" ++ s ++ "\""
