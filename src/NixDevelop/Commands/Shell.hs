{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}

module NixDevelop.Commands.Shell where

------------------------------------------------------------------------------
import           Control.Monad.Reader
import           Options.Applicative hiding (Success, Failure)
import           System.Exit
------------------------------------------------------------------------------
import           NixDevelop.NdApp
import           NixDevelop.NdShell
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.NdCommand
import           NixDevelop.Types.ShellOptions
------------------------------------------------------------------------------

------------------------------------------------------------------------------
shellOpts :: Parser ShellOptions
shellOpts = ShellOptions
      <$> (optional $ strOption
             ( long "run"
            <> metavar "CMD"
            <> help "Command to run in the shell" ))
      <*> (switch
             ( long "pure"
            <> help "Run in a sandbox without access to any of the current environment" ))
      <*> (switch
             ( long "reload"
            <> short 'r'
            <> help "Reload the environment from nix-shell (slower)" ))

shellMod :: Mod CommandFields NdCommand
shellMod = command "shell" $ info (Shell <$> shellOpts <**> helper) i
  where
    i = fullDesc
        <> header synopsis
        <> progDesc synopsis
    synopsis = "Open a nix shell with all the project dependencies available"


------------------------------------------------------------------------------
-- | Magical nix-develop shell command.  Caches the nix-shell environment to
-- make entering the shell much faster.
shellCommand :: ShellOptions -> NdApp ()
shellCommand opts = do
    ecode <- ndShell opts
    case ecode of
      Left lms -> mapM_ logMsg lms
      Right code -> liftIO $ exitWith code
