{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}

module NixDevelop.Commands.OldBuild where

------------------------------------------------------------------------------
import           Data.Default
import           Control.Error
import           Control.Monad.Trans
import           Control.Monad.Trans.Reader
import           Data.Fix
import           Data.Text (Text)
import           Nix.Expr
import           Nix.Parser
import           Options.Applicative hiding (Success, Failure)
import           System.Directory
import           System.Exit
import           System.Process
import           Text.Printf
------------------------------------------------------------------------------
import           NixDevelop.LanguageResolver
import           NixDevelop.Types.OldBuildOptions
import           NixDevelop.Types.Language
import           NixDevelop.Types.NdCommand
------------------------------------------------------------------------------


oldBuildMod :: Mod CommandFields NdCommand
oldBuildMod = command "old-build" $ info (OldBuild <$> parser <**> helper) i
  where
    parser = BuildOptions
      <$> switch
             ( short 'd'
            <> long "debug"
            <> help "Keep build information around for debugging" )
      <*> (many $ strOption
             ( long "arg"
            <> help "Argument to be passed to the build command.  Can be used multiple times." ))
    i = fullDesc
        <> header synopsis
        <> progDesc synopsis
    synopsis = "Build project"

------------------------------------------------------------------------------
-- | Executes an action making sure that the current directory is restored.
holdingCurDirConstant :: IO a -> IO ()
holdingCurDirConstant act = do
  curdir <- getCurrentDirectory
  _ <- act -- TODO Should probably bracket with exception handling
  setCurrentDirectory curdir

data BuildEnv = BuildEnv
  { buildEnvCmd  :: String
  , buildEnvOpts :: BuildOptions
  , buildEnvLang :: Maybe Language
  } deriving (Eq,Ord,Show,Read)


------------------------------------------------------------------------------
-- | Conventions:
--
-- If there is a default.nix file, we use that as the package's nix expression
-- If that nix expression needs arguments, we supply them with callPackage
-- Next we run the language resolver and use the default nix exrpession for
-- that language
oldBuildCommand :: BuildOptions -> IO ()
oldBuildCommand opts = do
    dir <- getCurrentDirectory
    lang <- resolveLanguage dir
    runReaderT (buildIn dir) $ BuildEnv "nix-build" opts lang

readLevel :: String -> Maybe Int
readLevel = readMay

buildIn :: FilePath -> ReaderT BuildEnv IO ()
buildIn directory = do
  hasNix <- liftIO $ doesFileExist "default.nix"
  if hasNix then nixBuild directory else languageBuild directory

nixExprArgsNeeded :: NExpr -> Int
nixExprArgsNeeded = cata phi
  where
    phi :: NExprF Int -> Int
    phi (NConstant _) = 0
    phi (NStr _) = 0
    phi (NSym _) = 0
    phi (NList _) = 0
    phi (NSet _) = 0
    phi (NRecSet _) = 0
    phi (NLiteralPath _) = 0
    phi (NEnvPath _) = 0
    phi (NUnary _ _) = 0
    phi (NBinary _ _ _) = 0
    phi (NSelect _ _ _) = 0
    phi (NHasAttr _ _) = 0
    phi (NAbs args _) = length $ funcArgsNames args
    phi (NLet _ _) = 0
    phi (NIf _ _ _) = 0
    phi (NWith _ _) = 0
    phi (NAssert _ _) = 0

funcArgsNames :: Params a -> [Text]
funcArgsNames (Param name) = [name]
funcArgsNames (ParamSet ps _ _) = map fst $ filter (isNothing . snd) ps

nixBuild :: FilePath -> ReaderT BuildEnv IO ()
nixBuild _ = do
    rExpr <- liftIO $ parseNixFile "default.nix"
    case rExpr of
      Failure e -> liftIO $ putStrLn "Parse error in default.nix" >> print e
      Success expr -> do
        let numArgs = nixExprArgsNeeded expr
        liftIO $ printf "Parsed default.nix into expression that takes %d args\n" numArgs
        if numArgs == 0
          then doNixBuild []
               -- TODO Might need to choose a language-specific callPackage function here
               -- For Haskell it has to be haskellPackages.callPackage
          else do
            cp <- asks (_lcCallPackage . maybe def globalLanguageConfig . buildEnvLang)
            doNixBuild ["-E", "(import <nixpkgs> {})." <> cp <> " ./default.nix {}"]

doNixBuild :: MonadIO m => [String] -> ReaderT BuildEnv m ()
doNixBuild args = do
    benv <- ask
    let bopts = buildEnvOpts benv
    let debugFunc = if buildOptionsDebug bopts then ("-K":) else id
    let finalArgs = debugFunc (buildOptionsArgs bopts) ++ args
    liftIO $ printf "Executing: %s %s\n" (buildEnvCmd benv) (unwords finalArgs)
    code <- liftIO $ rawSystem (buildEnvCmd benv) finalArgs
    case code of
      ExitSuccess -> return ()
      ExitFailure c -> liftIO $ printf "nix-develop failure %d in nix-build" c

languageBuild :: FilePath -> ReaderT BuildEnv IO ()
languageBuild _ = do
    asks buildEnvLang >>= \case
      Nothing -> liftIO $ putStrLn "error: Build failed.  Could not detect language."
      Just Haskell -> buildHaskell

buildHaskell :: ReaderT BuildEnv IO ()
buildHaskell = do
    cmd <- asks buildEnvCmd
    liftIO $ printf "Executing: %s %s\n" cmd (unwords args)
    _ <- liftIO $ rawSystem cmd args
    return ()
  where
    args = ["-E", "(import <nixpkgs> {}).haskellPackages.developPackage { root = ./.; }"]


------------------------------------------------------------------------------
-- Incremental Build
------------------------------------------------------------------------------

-- incrementalShellCmd =
--   [ mkdir -p tmp
--   , export NIX_BUILD_TOP="$PWD/tmp"
--   , export TMPDIR="$NIX_BUILD_TOP"
--   , export TEMPDIR="$NIX_BUILD_TOP"
--   , export TMP="$NIX_BUILD_TOP"
--   , export TEMP="$NIX_BUILD_TOP"
--   ,
--   , export HOME=/homeless-shelter
--   ,
--   , mkdir -p outputs
--   , for x in $outputs ; do
--   ,   mkdir -p "outputs/$x"
--   ,
--   ,   # Replace the expected output paths with our mutable output paths
--   ,   # everywhere they appear in the environment
--   ,   eval "$(set | sed "s@${!x//@/\@}@$PWD/outputs/$x@g")"
--   , done
--   ,
--   , # We want to overwrite old sources with new ones, but we don't want to change
--   , # any other files, e.g. build products that may have been created
--   , #TODO: Handle the case where sourceRoot *is* named 'src'
--   , eval "$(typeset -f unpackPhase | head -n -1 ; cat <<'ENDFUNC'
--   ,     if [ "$sourceRoot" != src ] ; then
--   ,         cp -aT "$sourceRoot" src
--   ,         rm -rf "$sourceRoot"
--   ,         sourceRoot=src
--   ,     fi
--   , }
--   , ENDFUNC
--   , )"
--   ,
--   , cd tmp
--   , genericBuild
--   ]
--
-- overrideSrcs =
--   [
--   ]
--
-- incrementalBuild :: String -> IO ()
-- incrementalBuild path = do
--     drv <- getDerivation path
--     let cmd = undefined
--     rawSystem "nix-shell"
--       [ "--pure"
--       , "-E", "import "++drv
--       , "--command", cmd
--       ]
--     system "ln -sf outputs/out result"
--
-- getDerivation path = do
--     (code, out, err) <- readProcessWithExitCode "nix-instantiate" [path] ""
--     return out
