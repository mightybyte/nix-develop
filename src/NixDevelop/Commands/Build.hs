{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}

module NixDevelop.Commands.Build where

------------------------------------------------------------------------------
import           Control.Error
import           Control.Monad.Reader
import qualified Data.Map as M
import qualified Data.Text as T
import           Data.Time
import qualified Data.Vector as V
import           Options.Applicative hiding (Success, Failure)
import           System.Exit
import           System.Process
import qualified Text.PrettyPrint.ANSI.Leijen as PP
import           Text.Printf
------------------------------------------------------------------------------
import           NixDevelop.NdApp
import           NixDevelop.Types.BuildOptions
import           NixDevelop.Types.BuildRecord
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.NdCommand
import           NixDevelop.Types.ProjConfig
------------------------------------------------------------------------------


buildMod :: Mod CommandFields NdCommand
buildMod = command "build" $ info (Build <$> parser <**> helper) i
  where
    parser = BuildOptions
      <$> (optional $ argument (maybeReader parseBuildTarget)
             ( metavar "BUILD LEVEL"
            <> help "A numeric build level from 1 to however many are defined in the config file."))
    i = fullDesc
        <> header synopsis
        <> progDesc synopsis
    synopsis = "Build project"

parseBuildTarget :: String -> Maybe BuildTarget
parseBuildTarget [] = Nothing
parseBuildTarget s = Just $
    case readMay s of
      Just aNat -> BuildLevel aNat
      Nothing -> BuildComponent s


------------------------------------------------------------------------------
-- | Conventions:
--
-- If there is a default.nix file, we use that as the package's nix expression
-- If that nix expression needs arguments, we supply them with callPackage.
-- Next we run the language resolver and use the default nix expression for
-- that language
buildCommand :: BuildOptions -> NdApp ()
buildCommand opts  = do
  (NdConfig _ _ _ _ pd pc _) <- ask
  case buildOptionsTarget opts of
    Nothing -> do
      logStr Debug "Building with no target"
      brs <- loadBuildRecords
      let level = case M.lookup UserBuild brs of
                    Nothing -> 1
                    Just br -> _buildRecordLevel br
      runBuildFromLevel AutomaticBuild pd pc level
    Just t -> do
      case t of
        BuildComponent c -> do
          logStr Fatal $ printf "Error: \"%s\" is not a valid build level!  Must be an int." c
        BuildLevel level -> do
          code <- runBuildLevel UserBuild pc (fromIntegral level)
          liftIO $ exitWith code

runBuildLevel :: BuildOrigin -> ProjConfig -> Int -> NdApp ExitCode
runBuildLevel o pc level = do
    -- TODO For now this is simply a shell command specified in the project config
    -- Eventually this will get more sophisticated.

    -- Users type a 1-based index so it's a left-to-right mneumonic
    -- The array, of course, is 0-based
    let cmd = T.unpack (_pcBuildLevels pc V.! (level - 1))
    logStr Info $ printf "Running level %d build" level
    logStr Detail cmd
    start <- liftIO getCurrentTime
    updateBuildRecord $ BuildRecord start o level Nothing
    logDoc Debug $ PP.text $ printf "Executing command: %s" cmd
    code <- liftIO $ system cmd
    end <- liftIO getCurrentTime
    let elapsed = realToFrac (diffUTCTime end start) :: Double
    logStr Info $ printf "Level %d build finished in %.1fs with %s\n"
                         level elapsed (show code)
    when (code == ExitSuccess) $
      updateBuildRecord $ BuildRecord end o level (Just code)
    return code

runBuildFromLevel :: BuildOrigin -> FilePath -> ProjConfig -> Int -> NdApp ()
runBuildFromLevel o projDir pc level = do
    code <- runBuildLevel o pc level
    case code of
      ExitFailure c ->
        logStr Fatal $ printf "Level %d build failed with code %d\n" level c
      ExitSuccess ->
        if level >= V.length (_pcBuildLevels pc)
          then return ()
          else runBuildFromLevel AutomaticBuild projDir pc (level + 1)

updateBuildRecord :: BuildRecord -> NdApp ()
updateBuildRecord br = do
    brs <- loadBuildRecords
    saveBuildRecords (M.insert (_buildRecordOrigin br) br brs)
