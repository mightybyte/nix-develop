{-# LANGUAGE OverloadedStrings #-}

module NixDevelop.NdShell where

------------------------------------------------------------------------------
import           Control.Error
import           Control.Monad.Trans
import qualified Data.Text as T
import           System.Exit
import           System.IO
import           System.Process
import qualified Text.PrettyPrint.ANSI.Leijen as PP
------------------------------------------------------------------------------
import           NixDevelop.ShellEnv
import           NixDevelop.NdApp
import           NixDevelop.Process
import           NixDevelop.Shells
import           NixDevelop.Types.LogMessage
import           NixDevelop.Types.NdApp
import           NixDevelop.Types.OsShellOptions
import           NixDevelop.Types.ShellOptions
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- | Haskell-level interface to nix-develop's shell command.
ndShell :: ShellOptions -> NdApp (Either [LogMessage] ExitCode)
ndShell so = do
  runExceptT $ do
    p <- ExceptT $ ndShellLowLevel so
    liftIO $ waitForProcess (_procHandle p)


------------------------------------------------------------------------------
-- | Haskell-level interface to nix-develop's shell command.  Returns the
-- contents of stdout as a String.
ndShellOut :: String -> NdApp (Either [LogMessage] (String, ExitCode))
ndShellOut cmd = do
  p <- liftIO $ startProcess $ (shell cmd)
    { env = Nothing
    , std_out = CreatePipe
    }
  case _procStdout p of
    Nothing -> return $ Left [msg Fatal $ T.pack
      "Could not get output of process (this should not happen)"]
    Just oh -> liftIO $ do
      out <- hGetContents oh
      ec <- waitForProcess (_procHandle p)
      return $ Right (out, ec)


------------------------------------------------------------------------------
-- | Haskell-level interface to nix-develop's shell command.
ndShellLowLevel :: ShellOptions -> NdApp (Either [LogMessage] Process)
ndShellLowLevel so = do
    res <- if soReload so
      then reloadNixShellEnv
      else reloadShellEnvIfNecessary
    case res of
      Left e ->
        return $ Left [LogMessage Fatal $ "There was a problem restoring environment:\n" <> T.pack (show e)]
      Right _ -> do
        let oso = OsShellOptions (soRun so) (soPure so)
        cp <- getShellCP oso
        logDoc Debug $ (PP.text "ndShell running the following command: " <>
                        PP.text (show $ cmdspec cp) <> PP.line)
        liftIO $ do
          p <- startProcess cp
          return (Right p)
