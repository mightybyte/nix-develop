name:                nix-develop
version:             0.1
synopsis:            Nix Universal Build Tool
-- description:
license:             BSD3
license-file:        LICENSE
author:              Doug Beardsley
maintainer:          mightybyte@gmail.com
copyright:           Doug Beardsley, Shea Levy
category:            Development
build-type:          Simple
extra-source-files:  ChangeLog.md
cabal-version:       >=1.10

source-repository head
  type:     git
  location: https://gitlab.com/mightybyte/nix-develop.git

Library
  hs-source-dirs: src

  exposed-modules:
    NixDevelop.Commands.Build
    NixDevelop.Commands.Docs
    NixDevelop.Commands.Init
    NixDevelop.Commands.OldBuild
    NixDevelop.Commands.Shell
    NixDevelop.Common
    NixDevelop.Directories
    NixDevelop.Globals
    NixDevelop.LanguageResolver
    NixDevelop.NdApp
    NixDevelop.NdShell
    NixDevelop.NixShell
    NixDevelop.Process
    NixDevelop.ShellEnv
    NixDevelop.Shells
    NixDevelop.Shells.Aliases
    NixDevelop.Shells.Bash
    NixDevelop.Shells.BashPrimitives
    NixDevelop.Shells.Common
    NixDevelop.Shells.EnvVars
    NixDevelop.Shells.Primitives
    NixDevelop.Shells.Zsh
    NixDevelop.Shells.ZshPrimitives
    NixDevelop.Utils
    NixDevelop.Types.BuildDesc
    NixDevelop.Types.BuildOptions
    NixDevelop.Types.BuildRecord
    NixDevelop.Types.DocsOptions
    NixDevelop.Types.GlobalOptions
    NixDevelop.Types.Language
    NixDevelop.Types.LogLevel
    NixDevelop.Types.LogMessage
    NixDevelop.Types.NdApp
    NixDevelop.Types.NdCommand
    NixDevelop.Types.NixCache
    NixDevelop.Types.OldBuildOptions
    NixDevelop.Types.OsShellOptions
    NixDevelop.Types.ProjConfig
    NixDevelop.Types.Shell
    NixDevelop.Types.ShellOptions
    NixDevelop.Types.UserConfig
  build-depends:
    aeson                  >= 1.2   && < 1.5,
    ansi-terminal          >= 0.8   && < 0.9,
    ansi-wl-pprint         >= 0.5   && < 0.7,
    base                   >= 4.9   && < 4.13,
    base64-bytestring      >= 1.0   && < 1.1,
    bytestring             >= 0.10  && < 0.11,
    containers             >= 0.5   && < 0.7,
    data-default           >= 0.7   && < 0.8,
    data-fix               >= 0.2   && < 0.3,
    directory              >= 1.3   && < 1.4,
    errors                 >= 2.2   && < 2.4,
    exceptions             >= 0.8   && < 0.11,
    filepath               >= 1.4   && < 1.5,
    free                   >= 5.0   && < 5.2,
    hashable               >= 1.2   && < 1.3,
    hnix                   >= 0.3   && < 0.6,
    lens                   >= 4.15  && < 4.18,
    lens-aeson             >= 1.0   && < 1.1,
    megaparsec             >= 7.0   && < 7.1,
    mtl                    >= 2.2   && < 2.3,
    neat-interpolation     >= 0.3   && < 0.4,
    network                >= 2.6   && < 2.7,
    network-socket-options >= 0.2   && < 0.3,
    optparse-applicative   >= 0.13  && < 0.15,
    process                >= 1.4   && < 1.7,
    string-conv            >= 0.1   && < 0.2,
    text                   >= 1.2   && < 1.3,
    these                  >= 0.7   && < 0.8,
    time                   >= 1.8   && < 1.10,
    transformers           >= 0.5   && < 0.6,
    turtle                 >= 1.5   && < 1.6,
    unix                   >= 2.7   && < 2.8,
    unordered-containers   >= 0.2.9 && < 0.3,
    uri-bytestring         >= 0.3   && < 0.4,
    vector                 >= 0.12  && < 0.13,
    zlib                   >= 0.6   && < 0.7
  default-language:    Haskell2010
  ghc-options: -Wall

executable nd
  main-is:             Main.hs
  build-depends:
    nix-develop,
    ansi-wl-pprint,
    base,
    optparse-applicative,
    text,
    transformers
  hs-source-dirs:      exe
  ghc-options: -Wall -threaded
  default-language:    Haskell2010

Test-suite nd-test
  hs-source-dirs: test/suite
  type: exitcode-stdio-1.0
  main-is: TestSuite.hs
  build-tool-depends: nix-develop:nd
  build-depends:
    aeson,
    ansi-terminal,
    ansi-wl-pprint,
    base,
    bytestring,
    data-default,
    directory,
    errors,
    exceptions,
    filepath,
    free,
    hashable,
    hnix,
    lens,
    lens-aeson,
    mtl,
    neat-interpolation,
    network,
    nix-develop,
    optparse-applicative,
    process,
    string-conv,
    tasty                  >= 1.1   && < 1.2,
    tasty-hunit            >= 0.10  && < 0.11,
    text,
    time,
    transformers,
    unix,
    unordered-containers,
    uri-bytestring,
    vector