{ compiler    ? "ghc844"
, rev         ? "3f3f6021593070330091a4a2bc785f6761bbb3c1"
, sha256      ? "1a7vvxxz8phff51vwsrdlsq5i70ig5hxvvb7lkm2lgwizgvpa6gv"
, runtests    ? false
  # The above pins taken from hnix
, pkgs        ? import (builtins.fetchTarball {
    url    = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";}) {
    config.allowBroken = false;
    config.allowUnfree = true;
  }
}:
let gitignore = pkgs.callPackage (pkgs.fetchFromGitHub {
      owner = "siers";
      repo = "nix-gitignore";
      rev = "4f2d85f2f1aa4c6bff2d9fcfd3caad443f35476e";
      sha256 = "1vzfi3i3fpl8wqs1yq95jzdi6cpaby80n8xwnwa8h2jvcw3j7kdz";
    }) {};
    callHackageDirect = {pkg, ver, sha256}@args:
      let pkgver = "${pkg}-${ver}";
      in pkgs.haskell.packages.${compiler}.callCabal2nix pkg (pkgs.fetchzip {
           url = "http://hackage.haskell.org/package/${pkgver}/${pkgver}.tar.gz";
           inherit sha256;
         });
in
pkgs.haskell.packages.${compiler}.developPackage {
  name = "nix-develop";
  root = gitignore.gitignoreSource [".git" ".gitlab-ci.yml" "README.md"] ./.;
  overrides = with pkgs.haskell.lib; self: super: {
    cryptohash-sha512 = doJailbreak super.cryptohash-sha512;
    hnix = dontCheck super.hnix;
    megaparsec = super.megaparsec_7_0_4;

    # For some strange reason, this works but the following doesn't.
    neat-interpolation = self.callCabal2nix "neat-interpolation" (pkgs.fetchFromGitHub {
      owner = "nikita-volkov";
      repo = "neat-interpolation";
      rev = "95c009643e89dd5db67d715078a007f7de79de27";
      sha256 = "0c7wqym619nq13xrf43w6bay0yl4jnxjaj4a0akmfw3srdcz07yf";
    }) {};

    # neat-interpolation = callHackageDirect {
    #   pkg = "neat-interpolation";
    #   ver = "0.3.2.4";
    #   sha256 = "0gygd2f0wbqa668dz7k8jfryilmbyzravlz7ysp3d13n8h00irba";
    # } {};

    serialise = doJailbreak (dontCheck super.serialise);

    ghc-datasize =
      overrideCabal super.ghc-datasize (attrs: {
        enableLibraryProfiling    = false;
        enableExecutableProfiling = false;
      });

    ghc-heap-view =
      overrideCabal super.ghc-heap-view (attrs: {
        enableLibraryProfiling    = false;
        enableExecutableProfiling = false;
      });

  };
  source-overrides = {
    # Use a specific hackage version
    # bytestring = "0.10.8.1";
    #
    # Use a particular commit from github
    # hnix = ../hnix;
    hnix = pkgs.fetchFromGitHub {
      owner = "haskell-nix";
      repo = "hnix";
      rev = "617d0867ab96c8f97b02c4524bd948d9f114005e";
      sha256 = "037kxj9wirynmavlp7d0k19a5xrhj117hlh3yia12xj6v828b99z";
      fetchSubmodules = true;
    };
    # unordered-containers = "0.2.9.0";
  };
  modifier = drv: pkgs.haskell.lib.overrideCabal drv (attrs: {
    buildTools = (attrs.buildTools or []) ++ [
      pkgs.haskell.packages.${compiler}.cabal-install
      pkgs.haskell.packages.${compiler}.ghcid

      # This is needed to run cabal v2-update in gitlab ci
      pkgs.wget
    ];

    # We need cabal new-test, but nix only runs ./Setup.hs test, so we have to
    # manually run our test suite in .gitlab-ci.yml instead of running it here.
    doCheck = runtests;
  });
}
