{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeApplications #-}

module Main where

------------------------------------------------------------------------------
import           Control.Error
import           Control.Monad.Catch
import           Control.Monad.Trans
import           Control.Monad.Trans.Except
import qualified Data.Aeson as A
import qualified Data.ByteString as B
import           Data.Default
import           Data.List
import           Data.Ord
import           Data.String.Conv
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Time
import           NeatInterpolation
import           Nix hiding (parse)
import           Nix.Convert
import           System.Directory
import           System.Environment
import           System.Exit
import           System.FilePath
import           System.Process
import           Test.Tasty
import           Test.Tasty.HUnit
import           Text.Printf
------------------------------------------------------------------------------
import           NixDevelop.NdApp
import           NixDevelop.NixShell
import           NixDevelop.Types.NixCache
import           NixDevelop.Types.ProjConfig
import           NixDevelop.Types.Shell
import           NixDevelop.Types.UserConfig
------------------------------------------------------------------------------

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
    [ projConfigTests
    , cacheTests
    -- TODO Fix e2e tests
    --, e2eCliTests
    ]

--unitTests = testGroup "Unit tests"
--  [ --testCase "nd shell" testShellEnv
--  ]

e2eCliTests = testGroup "e2e cli tests"
    [ assertSuccess "basic init and build" (testProject "test/projects/hello-haskell")
    ]

-- It is important to use --pure.  Running `nix-shell --run "env | sort"`
-- does not give the same result as running `nix-shell` and then manually
-- running `env | sort`.  But if you use --pure, it does.
--
-- It's starting to look like this test won't be reliable
testShellEnv = do
  (_,o1,_,p1) <- runInteractiveCommand "nix-shell --pure --run \"env | sort\""
  (_,o2,_,p2) <- runInteractiveCommand "nd shell -r --run \"env | sort\""
  waitForProcess p1
  waitForProcess p2
  out1 <- B.hGetContents o1
  out2 <- B.hGetContents o2
  assertEqual "nd shell has the same environment as nix-shell"
              out1 out2

projConfigTests = testGroup "nd.nix parser tests"
    [ pcTest "Empty set parses" "{}" (ProjConfig mempty def)
    , pcTest "Empty caches" "{ caches = []; }" (ProjConfig mempty def)
    , pcTest "Empty buildLevels" "{ buildLevels = []; }" (ProjConfig mempty def)
    , pcTest "Full paths don't parse" "{ shell = \"/bin/bash\"; }" (ProjConfig mempty def)
    , pcTest "Bash parses" "{ shell = \"bash\"; }" (ProjConfig mempty (UserConfig mempty (Just Bash)))
    ]

cacheTests = testGroup "cache tests"
    [ ctest "slashes" [unsafeUrlToNixCache "https://cache.nixos.org"]
        ([unsafeUrlToNixCache "https://cache.nixos.org/"], mempty)
    , ctest "slashes"
        [ unsafeUrlToNixCache "http://example.com"
        , unsafeUrlToNixCache "https://cache.nixos.org/"]
        ([unsafeUrlToNixCache "https://cache.nixos.org/"], [unsafeUrlToNixCache "http://example.com"])
    ]
  where
    ctest msg cs actual = testCase msg $ assertEqual msg (partitionTrusted v cs) actual
    Just v = A.decode $ toS [text|
{
"substituters":{"description":"The URIs of substituters (such as https://cache.nixos.org/).","value":["https://nixcache.reflex-frp.org","https://cache.nixos.org/"]},
"trusted-public-keys":{"description":"Trusted public keys for secure substitution.","value":["ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=","cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="]},
"trusted-substituters":{"description":"Disabled substituters that may be enabled via the substituters option by untrusted users.","value":["http://192.168.1.5"]},
"trusted-users":{"description":"Which users or groups are trusted to ask the daemon to do unsafe things.","value":["john"]}
}
|]

pcTest :: String -> Text -> ProjConfig -> TestTree
pcTest msg t pc = testCase msg $ do
  v <- parse t
  assertEqual msg (Just pc) v


parse :: Text -> IO (Maybe ProjConfig)
parse t = do
    now <- liftIO getCurrentTime
    let opts = defaultOptions now
    case parseNixTextLoc t of
      Failure e -> return Nothing
      Success expr -> do
          v <- runLazyM opts $
              catch (evaluateExpression Nothing nixEvalExprLoc
                                        normalForm expr) $ \case
                  NixException frames ->
                      errorWithoutStackTrace . show
                          =<< renderFrames @(NThunk (Lazy IO)) frames
          runLazyM opts $ fromValueMay v

assertSuccess :: String -> ExceptT Text IO a -> TestTree
assertSuccess nm action = testCase nm $ do
    res <- runExceptT action
    case res of
      Left msg -> assertFailure $ T.unpack msg
      Right _ -> assertBool nm True

scriptSystem :: Maybe FilePath -> String -> ExceptT Text IO ()
scriptSystem cwd_ cmd = ExceptT $ do
    (_,moh,_,p) <-
      createProcess $ (shell cmd)
        { cwd = cwd_
        , std_out = CreatePipe
        }
    ec <- waitForProcess p
    if ec == ExitSuccess
      then return $ Right ()
      else return $ Left $ T.pack $ printf "Got failure %s from: %s" (show ec) cmd

testProject :: String -> ExceptT Text IO ()
testProject templateDir = do
    liftIO $ putStrLn $ "testProject " ++ templateDir
    let parent = "test" </> "sandbox"
    let d = parent </> (takeBaseName templateDir)
    scriptSystem Nothing $ "rm -fr " <> d
    scriptIO $ createDirectoryIfMissing True parent
    scriptIO $ system $ unwords ["cp -R", templateDir, parent]

    -- "cabal v2-exec" doesn't work here because it's running from the current
    -- test project directory and that directory contains a .cabal file.
    liftIO $ print =<< lookupEnv "PATH"
    scriptSystem (Just d) "nd init"
    scriptSystem (Just d) "nd shell --run 'cabal build'"
