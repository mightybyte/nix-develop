# Nix-Develop

## Goal

The goal of nix-develop is to be a universal build tool that is simple and easy
to use. It is based on the idea of purely functional package management and the
Nix ecosystem. It should work for projects of all shapes and sizes from small
beginner projects to large multi-project and multi-language codebases.

## Guiding Principles

1. User experience and ease of use is prioritized above all else.
2. Aggressively minimize boilerplate and work out of the box with no extra
   configuration in as many situations as possible.
3. Discoverability - It should be obvious how to discover core functionality.
4. Language agnostic - Ultimately we want to work with any language that the Nix
   ecosystem supports.

## Current Status

Here are the things nix-develop gives you today:

* Cached nix-shell environment that makes entering an `nd shell` almost instant
  once you have run it once.

* Better binary cache handling with ability to specify custom per-project
  caches in `nd.nix`.

* `nd init` command that works out of the box with just about any Haskell cabal
  package. (Support for oth

* `nd docs` command that loads local docs for the exact versions of packages
  your project depends on.

* Support for multiple shells. (See
  [src/NixDevelop/Shells](src/NixDevelop/Shells) for the current list.)

* Full control over shell configuration via user-editable rc files in
  `$HOME/.config/nd/<SHELL>`.


## Motivation

The Nix ecosystem suffers from a severe usability problem.  Its adoption has
not been nearly as good as the idea itself merits.  The nix-develop project is
an attempt to fix this problem by being laser focused on user experience.
This leads us to axiom #1.

1. Nixifying a small project should only require the creation of a single file.

Requiring more than one file is just too complicated.  Tools like `cabal2nix`
generate fairly nice and simple `default.nix` files.  However, they cannot be
built with `nix-build`.  They require something like the following command:

```
nix-build --eval -E '(import <nixpkgs> {}).callPackage ./default.nix
```

This command is not easy to discover and definitely not acceptable as a
command you have to type often to build something.  Therefore, we have to
automate it.  The most obvious way to do that would be to change the default
behavior of nix-build, so that it does the equivalent of the above command.
Problem is, that’s probably a no-go due to backwards compatibility.  So what
are we to do?

Enter `nix-develop` (thanks to Shea Levy for the name).  However, just hacking
on a tool isn’t sufficient.  We need to carefully develop a comprehensive plan
for addressing all of Nix’s usability problems through this command line tool
and make sure all the pieces fit together in a nice cohesive whole.  Let’s
identify all the main tasks / user stories that people will use nix to
accomplish, then figure out necessary coding conventions and tools needed
to relentlessly optimize these tasks.

### User Stories

1. Nixify a small private package that will never be added to nixpkgs
2. Nixify a public package for inclusion in nixpkgs
3. Perform a full build of a package
4. Builds can be done offline if all the dependencies are present locally
5. Dependencies are always fetched first to maximize the amount of offline-capable time
6. Perform an incremental build of a package
7. Debug a build that is failing
8. Launch a REPL with the package’s code loaded
9. Drop into a shell with all of a package’s environment in scope
10. Run package test suite
11. Retrieve build artifacts even when build stage/target fails (Nix currently doesn’t support this to my knowledge)
12. Generate code coverage report
13. Generate package documentation
14. Run performance tests (measurements)
15. Check for performance regressions
16. Language-specific tools (i.e. hoogle) available
17. Multi-package and multi-language projects
18. Build package (and test) with multiple GHC versions
19. Automatically test with new GHC versions when they come out without having to commit anything new to the library
20. Trivially run a full CI build locally
21. Some kind of access to different nix build phases???

### Overall Requirements

1. MUST (possibly SHOULD?) be language agnostic, or at least make language-specific parts pluggable
2. SHOULD provide a way for languages to extend the infrastructure with language-specific features (i.e. Haskell might want an easy way to perform just type checking)

### Nixification Requirements

1. MUST require no more than one .nix file
2. MUST aggressively minimize the amount of boilerplate required in that .nix file
3. MUST be composable.  Packages defined with nix should be able to depend on other packages defined with nix without needing to repeat their nix expressions.
4. SHOULD work with zero .nix files if possible (for supported languages)
5. Necessary nix expressions SHOULD be simple declarative specifications of the project
6. Nixification of simple shell script projects SHOULD NOT require the creation of scripts like this.  Instead, there should be some kind of simple syntax for declaratively specifying which files in the source directory should be made available, the build command, and which output files should be copied where, etc.  Ideally the user shouldn’t need to know about shell variables like $pandoc, $src, and $out.  (Ultimately we probably have to allow some shell scripting, but if we do we should try to do it in a way that prevents mistakes from blowing away your whole hard drive like I’ve seen on one occasion.)  Or, if we can’t avoid them needing to know about them, then we need MUCH better documentation about all of that.  Currently this might be inferable from the stuff buried in the line nodes here, but that requires a very careful reading and it took me quite some time to spot it.
7. Nix expression infrastructure probably SHOULD have something like buildInputs to allow people to have various tools available in their environment (https://www.reddit.com/r/NixOS/comments/7h8np7/stack_or_cabal_which_one_is_more_preferable_for/dqspzsn/)

### Build Requirements

1. MUST be buildable with a single straightforward and easy to discover command
2. MUST have a mode that works incrementally for use during development
3. MUST be able to build with profiling
4. MUST have some kind of options for distinguishing different levels of production-similarity.  The important axes here are speed (perhaps a --fast option for quick builds) as well as strictness (different levels of warning strictness, linting, etc).  You want to be able to have faster and less strict builds easily available during development, and still have the slower and more strict builds easily available to run right before you push code.  The ideal UX isn’t immediately apparent.  Thought should be given of what people want across a variety of programming languages.
5. Incremental build probably SHOULD have an easy way to clear whatever build artifacts are cached, potentially on a granular basis.
6. Builds (buth full and incremental) probably SHOULD support multi-package projects / multiple targets and allow you to build either all targets or one single target.

### Research Tasks

1. Make a list of languages / build tools to include in the survey.
2. Do a survey of build tools across a variety of languages and make a comprehensive feature matrix collecting all the information.

### Build Tools

1. Ant
2. Maven
3. SBT (Scala Build Tool)
4. Bazel (Google)
5. Buck (Facebook)
6. Pants (https://www.pantsbuild.org/)
7. Mill (https://github.com/lihaoyi/mill)

## Universal Build Interface

Developer workflows are almost always in tension between the need to have a
quick build that gives rapid feedback and the need to make sure what you’re
doing passes CI.  It’s a spectrum of how long the build takes.  On one end is
the smallest and quickest build.  This could be something as simple as only
doing syntax checking or in a strongly typed language, only type checking.  On
the other end is the full CI build, whatever that means for your project.
That might include building with “-Wall -Werror”, running the test suite,
perhaps running lint, etc.  If the project is a web frontend, that might also
include minifying JS code with the Google Closure compiler and doing a long
compression pass with zopfli or something similar to get the absolute smallest
download for users.  These operations in particular can take a long time, so
you almost never want to do them during local development, but definitely do
want to do them in CI.  If you want them in CI, you also want to be able to
run them locally because if you don’t, the turnaround time to wait for the CI
results will slow you down.

This can all be unified into a nice interface with a simple idea.  The most
important thing is how long the build takes.  You really want to sort all
these different build variations on a single axis: time.  This generalizes
perfectly across all projects and languages.  The command to run the fastest
build will be:

```
nd build 1
```

The command to run the slowest build will be:

```
nd build 42
```

...or whatever the highest defined build level is for the project.  There also
might be a convenience alias like

```
nd build --ci
```

...that would always give you the last build level.


Once you have this single axis of build variants, you can then go a step
further with even more convenience.

```
nd build
```

This would keep track of the highest build level that finished successfully
and automatically choose the appropriate level.  If that succeeded, it could
automatically proceed to the next build level.


Some have suggested that this approach isn’t readable since you don’t know
what the levels are doing.  This is a valid point that can be addressed by
having a second lower-level interface that is more descriptive.  So in
addition to nd build 5, you could also have something like this:

```
nd build --run-tests --gen-docs --lint --compilers=ghc801,ghc822
```

These verbose flags would probably be something specified on a per-project (or
perhaps per-language) basis, and then the numeric build levels would be
defined in terms of them.  This gives the user the maximum amount of power and
convenience.

It’s unclear how much of this infrastructure will exist in nix-develop and how
much will exist in config files and nix expressions.  One possibility that
seems attractive is for the language and project level conventions to be
specified in nix / nixpkgs and for nix-develop to interrogate nix using a
well-defined API to get the information it needs to provide the above
lower-level interface.  Then it would use that and the per-project config file
to implement the high-level numeric scale interface.

### Config Files

The clear best choice for nix-develop config file syntax is to use Nix
expressions.  While the Nix expression language certainly has its problems,
its set syntax is actually quite nice for specifying config data.  And this
choice makes it much easier to ensure that users have enough power to
customize things for unusual situations.
