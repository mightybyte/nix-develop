let

inShell = cmd: "nd shell --run '${cmd}'";
fastopts = "-O0 --ghc-options=\"-fno-code -fno-break-on-exception -fno-break-on-error\"";

in {
  caches = [
    # { url = "https://example.com";
    # }
  ];

  buildLevels = [
    # Incremental levels
    (inShell "cabal new-build --enable-tests --disable-documentation ${fastopts}")
    (inShell "cabal new-build --enable-tests --disable-documentation")
    (inShell "cabal new-build --enable-tests")

    # Full builds...commented out for now because they might be better put somewhere else
    # "nix-build"
  ];
}
